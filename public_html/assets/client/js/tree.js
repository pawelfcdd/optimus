const debounce = (func, wait, immediate) => {
  let timeout;
  return () => {
    const context = this;
    const later = () => {
      timeout = null;
      if (!immediate) func.apply(context, {func, wait, immediate});
    };
    const callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, {func, wait, immediate});
  };
};

function random_id() {
  return Math.random().toString(36).replace(/[^a-z]+/g, '').substr(2, 10);
}

function draw() {
  let svg = '';
  const $tree = $('.b-tree');
  $tree.find('svg').remove();
  let last_point = 0;
  let svgW;
  const svgH = $tree.outerHeight();

  const {
    top: treeTop,
    left: treeLeft,
  } = $tree.offset();

  const $children_without_first = $('.b-child').slice(1);

  $children_without_first.each(function(index) {
    const $tree_child = $(this);
    const $parent_branch = $tree_child.closest('.b-branch');
    const $tree_child_label = $tree_child.children('.b-label');

    const x1 = $parent_branch.offset().left - treeLeft - $tree_child_label.outerWidth() / 2;
    const y1 = $parent_branch.offset().top - treeTop + $parent_branch.height() / 2;

    const length = $children_without_first.length;

    $tree_child.children('.b-label').each(function () {
      const $label = $(this);
      const x2 = $label.offset().left - treeLeft + $label.outerWidth() / 2;
      const y2 = $label.offset().top - treeTop + $label.outerHeight() / 2;

      const $label_offset_right = $label.offset().left - treeLeft + $label.outerWidth();
      last_point = $label_offset_right > last_point
        ? $label_offset_right
        : last_point;

      svg += `
        <line 
          id="${random_id()}_line"
          stroke-width="2px"
          stroke="rgb(0,0,0)"
          x1=${x1}
          x2=${x2}
          y1=${y1}
          y2=${y2}
        />
      `;
    });
  });

  // Fix SVG width issue if window width is smaller than tree.
  svgW = last_point;

  $tree.prepend(`<svg id="${random_id()}" width="${svgW}" height="${svgH}">${svg}</svg>`);
}

document.addEventListener('DOMContentLoaded', function () {
  draw();
  window.addEventListener('resize', debounce(() => draw(), 100, false), false);
});
