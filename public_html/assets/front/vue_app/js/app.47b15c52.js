(function (n) {
    function t(t) {
        for (var e, r, i = t[0], b = t[1], o = t[2], l = 0, _ = []; l < i.length; l++) r = i[l], a[r] && _.push(a[r][0]), a[r] = 0;
        for (e in b) Object.prototype.hasOwnProperty.call(b, e) && (n[e] = b[e]);
        c && c(t);
        while (_.length) _.shift()();
        return p.push.apply(p, o || []), s()
    }

    function s() {
        for (var n, t = 0; t < p.length; t++) {
            for (var s = p[t], e = !0, r = 1; r < s.length; r++) {
                var b = s[r];
                0 !== a[b] && (e = !1)
            }
            e && (p.splice(t--, 1), n = i(i.s = s[0]))
        }
        return n
    }

    var e = {}, a = {app: 0}, p = [];

    function r(n) {
        return i.p + "js/" + ({}[n] || n) + "." + {
            "chunk-2d0b25c5": "1e1e53a8",
            "chunk-2d21006b": "bf320661"
        }[n] + ".js"
    }

    function i(t) {
        if (e[t]) return e[t].exports;
        var s = e[t] = {i: t, l: !1, exports: {}};
        return n[t].call(s.exports, s, s.exports, i), s.l = !0, s.exports
    }

    i.e = function (n) {
        var t = [], s = a[n];
        if (0 !== s) if (s) t.push(s[2]); else {
            var e = new Promise(function (t, e) {
                s = a[n] = [t, e]
            });
            t.push(s[2] = e);
            var p, b = document.createElement("script");
            b.charset = "utf-8", b.timeout = 120, i.nc && b.setAttribute("nonce", i.nc), b.src = r(n), p = function (t) {
                b.onerror = b.onload = null, clearTimeout(o);
                var s = a[n];
                if (0 !== s) {
                    if (s) {
                        var e = t && ("load" === t.type ? "missing" : t.type), p = t && t.target && t.target.src,
                            r = new Error("Loading chunk " + n + " failed.\n(" + e + ": " + p + ")");
                        r.type = e, r.request = p, s[1](r)
                    }
                    a[n] = void 0
                }
            };
            var o = setTimeout(function () {
                p({type: "timeout", target: b})
            }, 12e4);
            b.onerror = b.onload = p, document.head.appendChild(b)
        }
        return Promise.all(t)
    }, i.m = n, i.c = e, i.d = function (n, t, s) {
        i.o(n, t) || Object.defineProperty(n, t, {enumerable: !0, get: s})
    }, i.r = function (n) {
        "undefined" !== typeof Symbol && Symbol.toStringTag && Object.defineProperty(n, Symbol.toStringTag, {value: "Module"}), Object.defineProperty(n, "__esModule", {value: !0})
    }, i.t = function (n, t) {
        if (1 & t && (n = i(n)), 8 & t) return n;
        if (4 & t && "object" === typeof n && n && n.__esModule) return n;
        var s = Object.create(null);
        if (i.r(s), Object.defineProperty(s, "default", {
            enumerable: !0,
            value: n
        }), 2 & t && "string" != typeof n) for (var e in n) i.d(s, e, function (t) {
            return n[t]
        }.bind(null, e));
        return s
    }, i.n = function (n) {
        var t = n && n.__esModule ? function () {
            return n["default"]
        } : function () {
            return n
        };
        return i.d(t, "a", t), t
    }, i.o = function (n, t) {
        return Object.prototype.hasOwnProperty.call(n, t)
    }, i.p = "", i.oe = function (n) {
        throw console.error(n), n
    };
    var b = window["webpackJsonp"] = window["webpackJsonp"] || [], o = b.push.bind(b);
    b.push = t, b = b.slice();
    for (var l = 0; l < b.length; l++) t(b[l]);
    var c = o;
    p.push([0, "chunk-vendors"]), s()
})({
    0: function (n, t, s) {
        n.exports = s("56d7")
    }, "56d7": function (n, t, s) {
        "use strict";
        s.r(t);
        s("cadf"), s("551c"), s("f751"), s("097d");
        var e = s("2b0e"), a = s("1881"), p = s.n(a), r = function () {
                var n = this, t = n.$createElement, s = n._self._c || t;
                return s("div", {
                    staticClass: "b-layout",
                    attrs: {id: "app"}
                }, [s("router-view"), s("modal", {
                    attrs: {
                        name: "login",
                        adaptive: !0,
                        height: "auto",
                        scrollable: !0
                    }
                }, [s("FormLogin")], 1), s("modal", {
                    attrs: {
                        name: "register",
                        adaptive: !0,
                        height: "auto",
                        scrollable: !0
                    }
                }, [s("FormRegister")], 1), s("modal", {
                    attrs: {
                        name: "consultation",
                        adaptive: !0,
                        height: "auto",
                        scrollable: !0
                    }
                }, [s("FormConsultation")], 1)], 1)
            }, i = [], b = function () {
                var n = this, t = n.$createElement, s = n._self._c || t;
                return s("div", {staticClass: "b-form"}, [s("div", {staticClass: "b-form__inner"}, [s("h2", {staticClass: "b-text _s_md _ta_c _mb"}, [n._v("\n      Вход на платформу OPTIMUS\n    ")]), s("form", {staticClass: "b-form__container"}, [n._m(0), n._m(1), n._m(2), s("div", {staticClass: "b-form__row"}, [s("button", {
                    staticClass: "b-button-link",
                    attrs: {type: "button"},
                    on: {click: n.register}
                }, [n._v("Регистрация")])])])])])
            }, o = [function () {
                var n = this, t = n.$createElement, s = n._self._c || t;
                return s("div", {staticClass: "b-form__row"}, [s("input", {
                    staticClass: "b-input",
                    attrs: {type: "text", placeholder: "Логин"}
                })])
            }, function () {
                var n = this, t = n.$createElement, s = n._self._c || t;
                return s("div", {staticClass: "b-form__row"}, [s("input", {
                    staticClass: "b-input",
                    attrs: {type: "password", placeholder: "Пароль"}
                })])
            }, function () {
                var n = this, t = n.$createElement, s = n._self._c || t;
                return s("div", {staticClass: "b-form__row"}, [s("button", {
                    staticClass: "b-button-arrow",
                    attrs: {type: "button"}
                }, [n._v("Войти")])])
            }], l = {
                methods: {
                    register: function () {
                        this.$modal.hide("login"), this.$modal.show("register")
                    }
                }
            }, c = l, _ = s("2877"), u = Object(_["a"])(c, b, o, !1, null, null, null), d = u.exports, m = function () {
                var n = this, t = n.$createElement, s = n._self._c || t;
                return s("div", {staticClass: "b-form"}, [s("div", {staticClass: "b-form__inner"}, [n._m(0), s("form", {staticClass: "b-form__container"}, [n._m(1), n._m(2), n._m(3), n._m(4), n._m(5), n._m(6), s("div", {staticClass: "b-form__row"}, [s("button", {
                    staticClass: "b-button-link",
                    attrs: {type: "button"},
                    on: {click: n.login}
                }, [n._v("Уже есть аккаунт?")])]), n._m(7)])])])
            }, v = [function () {
                var n = this, t = n.$createElement, s = n._self._c || t;
                return s("h2", {staticClass: "b-text _s_md _ta_c _mb"}, [n._v("\n      Добро пожаловать"), s("br"), n._v("\n      в команду OPTIMUS\n    ")])
            }, function () {
                var n = this, t = n.$createElement, s = n._self._c || t;
                return s("div", {staticClass: "b-form__row"}, [s("input", {
                    staticClass: "b-input",
                    attrs: {type: "email", placeholder: "Эл. почта"}
                })])
            }, function () {
                var n = this, t = n.$createElement, s = n._self._c || t;
                return s("div", {staticClass: "b-form__row"}, [s("input", {
                    staticClass: "b-input",
                    attrs: {type: "text", placeholder: "Логин"}
                })])
            }, function () {
                var n = this, t = n.$createElement, s = n._self._c || t;
                return s("div", {staticClass: "b-form__row"}, [s("input", {
                    staticClass: "b-input",
                    attrs: {type: "phone", placeholder: "Телефон"}
                })])
            }, function () {
                var n = this, t = n.$createElement, s = n._self._c || t;
                return s("div", {staticClass: "b-form__row"}, [s("input", {
                    staticClass: "b-input",
                    attrs: {type: "password", placeholder: "Пароль"}
                })])
            }, function () {
                var n = this, t = n.$createElement, s = n._self._c || t;
                return s("div", {staticClass: "b-form__row"}, [s("input", {
                    staticClass: "b-input",
                    attrs: {type: "number", placeholder: "Кто пригласил"}
                }), s("p", {staticClass: "b-form__text"}, [n._v("\n          Логин в цифровом виде\n        ")])])
            }, function () {
                var n = this, t = n.$createElement, s = n._self._c || t;
                return s("div", {staticClass: "b-form__row"}, [s("button", {
                    staticClass: "b-button-arrow",
                    attrs: {type: "button"}
                }, [n._v("Регистрация")])])
            }, function () {
                var n = this, t = n.$createElement, s = n._self._c || t;
                return s("div", {staticClass: "b-form__row"}, [s("p", {staticClass: "b-form__text"}, [n._v("\n          Нажимая «Зарегистрироваться»,"), s("br"), n._v("\n          я принимаю условия платформы Optimus.\n        ")])])
            }], h = {
                methods: {
                    login: function () {
                        this.$modal.hide("register"), this.$modal.show("login")
                    }
                }
            }, f = h, C = Object(_["a"])(f, m, v, !1, null, null, null), w = C.exports, g = function () {
                var n = this, t = n.$createElement;
                n._self._c;
                return n._m(0)
            }, x = [function () {
                var n = this, t = n.$createElement, s = n._self._c || t;
                return s("div", {staticClass: "b-form"}, [s("div", {staticClass: "b-form__inner"}, [s("h2", {staticClass: "b-text _s_md _ta_c _mb"}, [n._v("\n      Консультация\n    ")]), s("form", {staticClass: "b-form__container"}, [s("div", {staticClass: "b-form__row"}, [s("input", {
                    staticClass: "b-input",
                    attrs: {type: "text", placeholder: "Имя"}
                })]), s("div", {staticClass: "b-form__row"}, [s("input", {
                    staticClass: "b-input",
                    attrs: {type: "phone", placeholder: "Телефон"}
                })]), s("div", {staticClass: "b-form__row"}, [s("input", {
                    staticClass: "b-input",
                    attrs: {type: "email", placeholder: "Эл. почта"}
                })]), s("div", {staticClass: "b-form__row"}, [s("button", {
                    staticClass: "b-button-arrow",
                    attrs: {type: "button"}
                }, [n._v("Отправить")])]), s("div", {staticClass: "b-form__row"}, [s("p", {staticClass: "b-form__text"}, [n._v("\n          Нажимая на кнопку «Отправить»,"), s("br"), n._v("\n          вы даете согласие на обработку своих персональных данных.\n        ")])])])])])
            }], $ = {
                methods: {
                    register: function () {
                        this.$modal.hide("login"), this.$modal.show("register")
                    }
                }
            }, y = $, E = Object(_["a"])(y, g, x, !1, null, null, null), k = E.exports,
            O = {components: {FormLogin: d, FormRegister: w, FormConsultation: k}}, P = O,
            j = (s("5c0b"), Object(_["a"])(P, r, i, !1, null, null, null)), z = j.exports, M = s("8c4f"),
            T = function () {
                var n = this, t = n.$createElement, s = n._self._c || t;
                return s("Base", [s("section", {staticClass: "b-home"}, [s("div", {staticClass: "b-home__inner"}, [s("h1", {staticClass: "b-title"}, [n._v("\n        Бизнес-платформа"), s("br"), n._v("\n        OPTIMUS\n      ")]), s("p", {staticClass: "b-text _s_lg _max-width _mb"}, [n._v("\n        Платформа предоставляет рабочие стратегии ведения продаж\n        на популярной международной торговой площадке "), s("strong", [n._v("Amazom.com")]), n._v(".\n        Наши инструменты помогут вам достичь финансовой независимости.\n      ")]), s("button", {
                    staticClass: "b-button-arrow",
                    attrs: {type: "button"},
                    on: {
                        click: function (t) {
                            return n.$modal.show("consultation")
                        }
                    }
                }, [s("span", [n._v("Бесплатная консультация")])])])]), s("section", {staticClass: "b-block-centered"}, [s("div", {staticClass: "b-block-centered__inner"}, [s("h2", {staticClass: "b-title b-text _ta_c"}, [n._v("\n        Принцип работы"), s("br"), n._v("\n        нейросети OPTIMUS\n      ")]), s("p", {staticClass: "b-text _s_md _max-width_lg _ta_c"}, [n._v("\n        Уникальная система автоматизированных продаж, разработанная специально для площадки\n        "), s("strong", [n._v("Аmazom.com")]), n._v(":\n        ищет товары, поставщиков, просчитывает логистику, создает листинги и ведет продажи. Процесс полностью\n        автоматизирован, происходит практически без вашего участия. Гарантируем вам стабильные доходы уже с первого\n        месяца использования инструмента.\n      ")]), s("p", {staticClass: "b-text _s_lg _color_accent _max-width_lg _ta_c"}, [n._v("\n        Просто. Прозрачно. Надежно.\n      ")]), s("div", {staticClass: "b-icons-chain"}, [s("div", {staticClass: "b-icons-chain__item _find-goods"}, [n._v("\n          Поиск товара\n        ")]), s("div", {staticClass: "b-icons-chain__item _find-supplier"}, [n._v("\n          Поиск поставщика\n        ")]), s("div", {staticClass: "b-icons-chain__item _buy"}, [n._v("\n          Закуп товара\n        ")]), s("div", {staticClass: "b-icons-chain__item _send"}, [n._v("\n          Логистика\n        ")]), s("div", {staticClass: "b-icons-chain__item _sale"}, [n._v("\n          Продажа\n        ")])])])]), s("section", {staticClass: "b-catalog"}, [s("div", {staticClass: "b-catalog__inner"}, [s("div", {staticClass: "b-catalog__columns"}, [s("div", {staticClass: "b-catalog__text"}, [s("h2", {staticClass: "b-title"}, [n._v("\n            Ассортимент"), s("br"), n._v("\n            продуктов\n          ")]), s("p", {staticClass: "b-text _s_md _max-width"}, [n._v("\n            Нейросеть «OPTIMUS» обеспечит вам постоянный доход за счет уникального алгоритма ведения бизнеса.\n          ")]), s("p", {staticClass: "b-text _s_md _max-width"}, [n._v("\n            Образовательные курсы по запуску своего бизнеса торговой площадке amazom.com. Разработаны уникальные\n            методики ведения продаж и проверенные на личном опыте.\n          ")])]), s("div", {staticClass: "b-catalog__products"}, [s("Products", {attrs: {size: "small"}})], 1)])])]), s("Reviews")], 1)
            }, L = [], A = s("bd0c"), I = s("7079"), S = function () {
                var n = this, t = n.$createElement, s = n._self._c || t;
                return s("div", {staticClass: "b-reviews"}, [s("h2", {staticClass: "b-title b-text _ta_c"}, [n._v("\n    Отзывы\n  ")]), s("nav", {staticClass: "b-reviews__nav"}, [s("button", {
                    staticClass: "b-button-icon _arrow-left",
                    attrs: {type: "button"},
                    on: {
                        click: function (t) {
                            return n.goto(-1)
                        }
                    }
                }), s("button", {
                    staticClass: "b-button-icon _arrow-right",
                    attrs: {type: "button"},
                    on: {
                        click: function (t) {
                            return n.goto(1)
                        }
                    }
                })]), s("div", {staticClass: "b-reviews__inner"}, [s("div", {staticClass: "b-reviews__row"}, n._l(n.visible, function (t, e) {
                    return s("div", {
                        key: e,
                        staticClass: "b-review",
                        class: "_" + (e + 1 + n.current_index)
                    }, [s("div", {staticClass: "b-review__inner"}, [s("p", {staticClass: "b-text _s_md"}, [n._v("\n            " + n._s(t.name) + "\n          ")]), s("p", [n._v("\n            " + n._s(t.date) + "\n          ")]), s("div", {
                        staticClass: "b-review__text",
                        domProps: {innerHTML: n._s(t.text)}
                    })])])
                }), 0)])])
            }, q = [], F = {
                data: function () {
                    return {
                        current_index: 0,
                        step: 3,
                        reviews: [{
                            name: "Buks",
                            date: "19.05.2019",
                            text: "\n<p>С&nbsp;этим проектом я&nbsp;познакомился 2&nbsp;месяца назад и&nbsp;за&nbsp;это время я&nbsp;много раз убедился в&nbsp;его качестве. В&nbsp;нём есть возможности для заработка. </p>\n<p>Проект работает 4-ый месяц и&nbsp;я&nbsp;очень надеюсь, что он&nbsp;будет работать ещё очень долго. Классный проект!</p>\n            "
                        }, {
                            name: "Oksi2412",
                            date: "06.06.2019",
                            text: "\n<p>Наконец-то мне встретился проект с&nbsp;большими возможностями. </p>\n<p>Я&nbsp;внимательно ознакомилась с&nbsp;разными вариантами заработка. </p>\n<p>Тема продажи товаров на&nbsp;амазон мне очень нравиться я&nbsp;купила нейросеть и&nbsp;не&nbsp;жалею об&nbsp;этом. Сейчас проходу обучение и&nbsp;скоро начну еще сама продавать это просто супер!</p>\n            "
                        }, {
                            name: "Юрий Кузнецов",
                            date: "05.05.2019",
                            text: "\n<p>Очередная быстрая выплата от&nbsp;Optimus! </p>\n<p>Проект стабильно выплачивает. </p>\n<p>Идет все супер! Благодаря уверенным продажам товара, а&nbsp;выплаты ежедневно поступают на&nbsp;баланс ЛК </p>\n            "
                        }, {
                            name: "igoryanuch",
                            date: "30.04.2019",
                            text: "\n<p>Для тех, кто сомневается&nbsp;и... кто до&nbsp;сих пор ищет возможность заработка в&nbsp;сети(раз уж&nbsp;вы&nbsp;настолько заинтересовались &laquo;Optimus&raquo;, что ищете и&nbsp;читаете/смотрите отзывы о&nbsp;данном сообществе)- <br />\n  примите мои поздравления! Вы&nbsp;нашли достойный проект, который работает и&nbsp;платит своим участникам. Вы, уже на&nbsp;пол шага к&nbsp;своей финансовой независимости! <br />\n  <br />\n  Чем раньше вы&nbsp;присоединитесь и&nbsp;распределите свои средства, тем ранее начнёте получать прибыль! <br />\n  <br />\n  Не&nbsp;теряйте свои деньги в&nbsp;&quot;хайпах&quot;,&quot;лохотронах&quot; и&nbsp;&laquo;баблосборниках&raquo;! <br />\n  <br />\n  Цените своё время! </p>\n            "
                        }, {
                            name: "Levft",
                            date: "18.06.2019",
                            text: "\n<p>\n  Реально интересный сайт. Команда всегда на&nbsp;месте, грамотно отвечает на&nbsp;любые вопросы и&nbsp;уже это большой плюс. Можно покупать разные пакеты и&nbsp;получать проценты, можно покупать знания и&nbsp;строить собственный бизнес на&nbsp;Amazon. Нейросеть, обучение, выплаты, всё это прекрасно дополняет проект. Рекомендую грамотным пользователям.</p>\n\n            "
                        }, {
                            name: "alexander955",
                            date: "04.10.18",
                            text: "\n<p>Приветствую всех! Интересный сайт, только я&nbsp;ничего не&nbsp;понял, поэтому прошу создателей сайта пояснить мне все! Хочу купить обучение и&nbsp;начать свой бизнес на&nbsp;Amazon.</p>\n            "
                        }]
                    }
                }, computed: {
                    visible: function () {
                        return this.reviews.slice(this.current_index, this.current_index + this.step)
                    }
                }, methods: {
                    goto: function (n) {
                        n = 1 === n ? this.current_index + this.step : this.current_index - this.step, n < 0 && (n = this.reviews.length - this.step), n + 1 > this.reviews.length && (n = 0), this.current_index = n
                    }
                }
            }, R = F, B = Object(_["a"])(R, S, q, !1, null, null, null), H = B.exports,
            U = {components: {Base: A["a"], Products: I["a"], Reviews: H}}, N = U,
            D = Object(_["a"])(N, T, L, !1, null, null, null), J = D.exports;
        e["a"].use(M["a"]);
        var V = new M["a"]({
            mode: "history",
            base: "",
            routes: [{path: "/", name: "home", component: J}, {
                path: "/catalog",
                name: "catalog",
                component: function () {
                    return s.e("chunk-2d0b25c5").then(s.bind(null, "247d"))
                }
            }, {
                path: "/partnership", name: "partnership", component: function () {
                    return s.e("chunk-2d21006b").then(s.bind(null, "b5ac"))
                }
            }]
        });
        V.afterEach(function () {
            window.scrollTo(0, 0)
        });
        var X = V;
        e["a"].use(p.a), e["a"].config.productionTip = !1, new e["a"]({
            router: X, render: function (n) {
                return n(z)
            }, mounted: function () {
                return document.dispatchEvent(new Event("x-app-rendered"))
            }
        }).$mount("#app")
    }, "5c0b": function (n, t, s) {
        "use strict";
        var e = s("5e27"), a = s.n(e);
        a.a
    }, "5e27": function (n, t, s) {
    }, 7079: function (n, t, s) {
        "use strict";
        var e = function () {
            var n = this, t = n.$createElement, s = n._self._c || t;
            return s("div", {staticClass: "b-products"}, [s("nav", {
                staticClass: "b-products__nav",
                class: {_wide: "full" === n.size}
            }, [s("button", {
                staticClass: "b-products__sort _neural-network",
                class: {_current: n.current_index < n.second_block_index},
                attrs: {type: "button"},
                on: {
                    click: function (t) {
                        return n.goto(0)
                    }
                }
            }, [s("span", [n._v("Нейросеть")])]), s("div", {staticClass: "b-products__sort-divider"}), s("button", {
                staticClass: "b-products__sort _education",
                class: {_current: n.current_index >= n.second_block_index},
                attrs: {type: "button"},
                on: {
                    click: function (t) {
                        return n.goto(n.second_block_index)
                    }
                }
            }, [s("span", [n._v("Образование")])])]), s("div", {staticClass: "b-products__card"}, ["full" === n.size ? s("div", {staticClass: "b-product__background"}) : n._e(), n._l(n.products, function (t, e) {
                return e === n.current_index ? s("div", {
                    key: e,
                    staticClass: "b-product"
                }, [s("div", {
                    staticClass: "b-product__slider",
                    class: {_wide: "full" === n.size}
                }, [s("div", {
                    staticClass: "b-product__image",
                    class: ["_" + t.name, "full" === n.size ? "_clean" : ""]
                }, [s("div", {staticClass: "b-product__arrows"}, [s("button", {
                    staticClass: "b-button-icon _arrow-left",
                    attrs: {type: "button"},
                    on: {
                        click: function (t) {
                            return n.goto(-1, !0)
                        }
                    }
                }), s("button", {
                    staticClass: "b-button-icon _arrow-right",
                    attrs: {type: "button"},
                    on: {
                        click: function (t) {
                            return n.goto(1, !0)
                        }
                    }
                })])]), s("aside", {
                    staticClass: "b-product__sidebar",
                    class: {_wide: "full" === n.size}
                }, [s("div", {staticClass: "b-sidebar__row"}, [s("div", {staticClass: "b-text _s_lg"}, [n._v("\n              " + n._s(t.label) + "\n            ")])]), s("div", {staticClass: "b-sidebar__row"}, [s("p", {staticClass: "b-text _s_md _fw_md"}, [n._v("\n              Что входит:\n            ")]), t.sidebar_table ? s("table", {staticClass: "b-product__table"}, n._l(t.sidebar_table, function (t, e) {
                    return s("tr", {key: e}, [s("td", [n._v(n._s(t.label))]), s("td", [n._v(n._s(t.value))])])
                }), 0) : n._e()]), s("div", {staticClass: "b-sidebar__row"}, [s("div", {staticClass: "b-cols _mt"}, [n._m(0, !0), t.price ? s("div", {staticClass: "b-col"}, [s("span", [n._v("Стоимость")]), s("br"), s("span", {staticClass: "b-text _s_lg _fw_md"}, [n._v(n._s(t.price))])]) : n._e()])])])]), "full" === n.size && t.description ? s("div", {staticClass: "b-product__description"}, [s("div", {staticClass: "b-text _max-width_lg _s_md"}, [s("p", {staticClass: "b-title _s_md"}, [n._v("\n            Описание продукта\n          ")]), s("div", {domProps: {innerHTML: n._s(t.description.a)}}), s("div", {staticClass: "b-row"}, [s("router-link", {
                    staticClass: "b-button-arrow",
                    attrs: {to: {name: "partnership"}}
                }, [n._v("\n              Сотрудничество\n            ")])], 1), s("div", {domProps: {innerHTML: n._s(t.description.b)}})])]) : n._e()]) : n._e()
            })], 2)])
        }, a = [function () {
            var n = this, t = n.$createElement, s = n._self._c || t;
            return s("div", {staticClass: "b-col"}, [s("button", {
                staticClass: "b-button-order",
                attrs: {type: "button"}
            }, [n._v("\n                  Заказать\n                ")])])
        }], p = {
            props: ["size"], data: function () {
                return {
                    current_index: 0,
                    second_block_index: 3,
                    products: [{
                        name: "expert",
                        label: "Expert",
                        price: "3 000 $",
                        sidebar_table: [{label: "Количество товаров:", value: "2—4"}, {
                            label: "Срок аренды нейросети:",
                            value: "12 месяцев"
                        }, {label: "Средний профит в месяц:", value: "7—9%"}],
                        description: {
                            a: "\n<p>E&nbsp;X&nbsp;P E&nbsp;R&nbsp;T&nbsp;&mdash; 3000$ цена продукта</p>\n<p>Количество товаров: 2&nbsp;&mdash; 4 </p>\n<p>В&nbsp;представленном пакете нейросеть подбирает для вас от&nbsp;2&nbsp;до&nbsp;4&nbsp;товаров для успешной торговли на&nbsp;Amazon.com\nПроизводство товара и&nbsp;доставка из&nbsp;Китая в&nbsp;Америку занимает от&nbsp;15-30 дней</p>\n<p>Как только товар окажется на&nbsp;складе Амазон нейросеть начинает выставлять продукцию на&nbsp;продажу. </p>\n<p>Первая выплата выплачивается на&nbsp;35-45 день с&nbsp;момента оплаты продукта. В&nbsp;дальнейшем выплата формируется 25-30 числа каждого месяца</p>\n<p>Ваш депозит постоянно находиться в&nbsp;обороте на&nbsp;закупке товаров что позволяет сохранить и&nbsp;приумножить ваши деньги.</p>\n<p>За&nbsp;всем процессом можно следить из&nbsp;личного кабинета.</p>\n<p>ПРОСТО. ПРОЗРАЧНО. НАДЕЖНО.</p>\n              ",
                            b: "\n<p>Личное приглашение&nbsp;10% от&nbsp;цены проданного продукта</p>\n<p>Командный бонус&nbsp;7% от&nbsp;меньшей цены из&nbsp;сформированной пары</p>\n              "
                        }
                    }, {
                        name: "middle",
                        label: "Middle",
                        price: "2 000 $",
                        sidebar_table: [{label: "Количество товаров:", value: "1—3"}, {
                            label: "Срок аренды нейросети:",
                            value: "12 месяцев"
                        }, {label: "Средний профит в месяц:", value: "6—8%"}],
                        description: {
                            a: "\n<p>M&nbsp;I&nbsp;D&nbsp;D&nbsp;L&nbsp;E&nbsp;&mdash; 2000$ цена продукта</p>\n<p>Количество товаров: 2&nbsp;&mdash; 3</p>\n<p>В&nbsp;представленном пакете нейросеть подбирает для вас от&nbsp;2&nbsp;до&nbsp;3&nbsp;товаров для успешной торговли на&nbsp;Amazon.com </p>\n<p>Производство товара и&nbsp;доставка из&nbsp;Китая в&nbsp;Америку занимает от&nbsp;15-30 дней</p>\n<p>Как только товар окажется на&nbsp;складе Амазон нейросеть начинает выставлять продукцию на&nbsp;продажу. </p>\n<p>Первая выплата выплачивается на&nbsp;35-45 день с&nbsp;момента оплаты продукта. В&nbsp;дальнейшем выплата формируется 25-30 числа каждого месяца</p>\n<p>Ваш депозит постоянно находиться в&nbsp;обороте на&nbsp;закупке товаров что позволяет сохранить и&nbsp;приумножить ваши финансы.</p>\n<p>За&nbsp;всем процессом можно следить из&nbsp;личного кабинета.</p>\n<p>ПРОСТО. ПРОЗРАЧНО. НАДЕЖНО.</p>\n              ",
                            b: "\n<p>Личное приглашение&nbsp;8% от&nbsp;цены проданного продукта</p>\n<p>Командный бонус&nbsp;6% от&nbsp;меньшей цены из&nbsp;сформированной пары</p>\n              "
                        }
                    }, {
                        name: "minimal",
                        label: "Minimal",
                        price: "1 000 $",
                        sidebar_table: [{label: "Количество товаров:", value: "1"}, {
                            label: "Срок аренды нейросети:",
                            value: "12 месяцев"
                        }, {label: "Средний профит в месяц:", value: "5—6%"}],
                        description: {
                            a: "\n<p>M&nbsp;I&nbsp;N I&nbsp;M&nbsp;A L&nbsp;&mdash; 1000$ цена продукта</p>\n<p>Количество товаров: 1</p>\n<p>В&nbsp;представленном пакете нейросеть подбирает для вас 1&nbsp;товар для успешной торговли на&nbsp;Amazon.com </p>\n<p>Производство товара и&nbsp;доставка из&nbsp;Китая в&nbsp;Америку занимает от&nbsp;15-30 дней</p>\n<p>Как только товар окажется на&nbsp;складе Амазон нейросеть начинает выставлять продукцию на&nbsp;продажу. </p>\n<p>Первая выплата выплачивается на&nbsp;35-45 день с&nbsp;момента оплаты продукта. В&nbsp;дальнейшем выплата формируется 25-30 числа каждого месяца</p>\n<p>Ваш депозит постоянно находиться в&nbsp;обороте на&nbsp;закупке товаров что позволяет сохранить и&nbsp;приумножить ваши финансы.</p>\n<p>За&nbsp;всем процессом можно следить из&nbsp;личного кабинета.</p>\n<p>ПРОСТО. ПРОЗРАЧНО. НАДЕЖНО.</p>\n              ",
                            b: "\n<p>Личное приглашение&nbsp;7% от&nbsp;цены проданного продукта</p>\n<p>Командный бонус&nbsp;5% от&nbsp;меньшей цены из&nbsp;сформированной пары</p>\n              "
                        }
                    }, {
                        name: "private_label",
                        label: "Private label",
                        price: "2 000 $",
                        sidebar_table: [{label: "Срок обучения:", value: "1 месяц"}, {
                            label: "Бизнес профит в месяц:",
                            value: "от 20% от оборота"
                        }],
                        description: {
                            a: "\n<p>P&nbsp;R&nbsp;I&nbsp;V&nbsp;A T&nbsp;E&nbsp;L&nbsp;A B&nbsp;E&nbsp;L&nbsp;&mdash; 2000$ цена обучения</p>\n<p>Срок обучения: 30&nbsp;дней</p>\n<p>Это бизнес, который связан с&nbsp;долгосрочными инвестициями, созданием собственного бренда, выводом линейки продуктов под едиными брендом. Что&nbsp;же такое P&nbsp;R&nbsp;I&nbsp;V&nbsp;A T&nbsp;E&nbsp;L&nbsp;A B&nbsp;E&nbsp;L&nbsp;по&nbsp;своей сути и&nbsp;как это происходит?</p>\n<p>Вы&nbsp;анализируете наиболее продающиеся товары на&nbsp;Амазон.<br />\nОпределяетесь с&nbsp;нишей и&nbsp;товаром.<br />\nОпределяете улучшения и&nbsp;доработки продукта, если считаете, что так он&nbsp;будет продаваться лучше (изменение формы, материала, технические детали).</p>\n<p>На&nbsp;этом этапе важно понимать, что вы&nbsp;не&nbsp;нарушаете ничьих патентов или других форм права интеллектуальной собственности.<br />\nЗаказываете производство выбранного продукта (чаще всего&nbsp;&mdash; Китай, но&nbsp;это также может быть и&nbsp;ваш собственный товар, производителем которого вы&nbsp;являетесь в&nbsp;своей стране).</p>\n<p>Создаете свою торговую марку и&nbsp;дизайн продукта и&nbsp;упаковки.<br />\nСоздаете продающий листинг (здесь понадобятся специалисты в&nbsp;маркетинге и&nbsp;SEO-оптимизации) с&nbsp;качественными фотографиями и&nbsp;описанием продукта, чтобы его хотелось купить.</p>\n<p>Завозите первую партию товара (скажем, 100-500&nbsp;шт.&nbsp;в&nbsp;зависимости от&nbsp;самого товара) на&nbsp;Амазон и&nbsp;включаете все механизмы его продвижения&nbsp;&mdash; отзывы, рекламу, раздачи, социальный маркетинг и&nbsp;т.&nbsp;д.<br />\nПолучаете первые продажи. Получаете свои первые органические отзывы на&nbsp;продукт и&nbsp;в&nbsp;зависимости от&nbsp;них&nbsp;&mdash; дорабатываете его или оставляете как есть (если всем нравится).</p>\n<p>Заказываете следующую партию товара (в&nbsp;два-три раза большую, чем пробная) на&nbsp;полученную прибыль.</p>\n<p>В&nbsp;зависимости от&nbsp;скорости роста продаж&nbsp;&mdash; реинвестируете в&nbsp;новые партии товара, чтобы поддерживать спрос и&nbsp;обеспечивать продажи.<br />\nИ&nbsp;вот только теперь получаете заслуженные лавры победителя&nbsp;&mdash; стабильные продажи вашего продукта, стабильную прибыль и&nbsp;налаженный логистический и&nbsp;производственный процессы.</p>\n<p>Как видите, даже основные шаги по&nbsp;созданию такого вида бизнеса&nbsp;&mdash; это не&nbsp;легкая прогулка по&nbsp;широкому океану возможностей Амазон. Это сложная работа, требующая целеустремленности и&nbsp;концентрации на&nbsp;всех этапах. Но&nbsp;и&nbsp;приз в&nbsp;этой игре достаточно высок&nbsp;&mdash; ваш защищенный бизнес на&nbsp;Амазон, который вы&nbsp;можете масштабировать, развить, а&nbsp;если захотите&nbsp;&mdash; даже продать. Действительно, такой бизнес может быть с&nbsp;легкостью продан, когда вы&nbsp;выведете его на&nbsp;стабильную прибыль. Или&nbsp;же, если вас не&nbsp;страшат высокие вложения, исчисляемые в&nbsp;десятках, а&nbsp;то&nbsp;и&nbsp;сотнях тысяч долларов&nbsp;&mdash; можете такой бизнес купить уже готовым.<br />\nМы&nbsp;поможем вам овладеть всей информацией для успешного старта вашего бизнеса.</p>\n<p>Начинать этот бизнес можно с&nbsp;суммы: $3,000 </p>\n<p>Предпочтительная сумма на&nbsp;старте: $5,000&nbsp;&mdash; $7,000</p>\n<p>Сроки выхода на&nbsp;окупаемость: 10-15 месяцев</p>\n<p>Потенциальная прибыль через год: $40,000-50,000&nbsp;в месяц</p>\n<p>Маржинальность: от&nbsp;20% и&nbsp;выше</p>\n              ",
                            b: "\n<p>Личное приглашение&nbsp;8% от&nbsp;цены проданного продукта</p>\n<p>Командный бонус&nbsp;6% от&nbsp;меньшей цены из&nbsp;сформированной пары</p>\n              "
                        }
                    }, {
                        name: "online_arbitrage",
                        label: "Online Арбитраж",
                        price: "2 000 $",
                        sidebar_table: [{label: "Срок обучения:", value: "1 месяц"}, {
                            label: "Бизнес профит в месяц:",
                            value: "от 8% от оборота"
                        }],
                        description: {
                            a: "\n<p>O&nbsp;N&nbsp;L&nbsp;I&nbsp;N E&nbsp;А&nbsp;Р&nbsp;Б&nbsp;И&nbsp;Т&nbsp;Р&nbsp;А&nbsp;Ж&nbsp;&mdash; 1000$ цена обучения</p>\n<p>Срок обучения 30&nbsp;дней</p>\n<p>Этот способ продажи на&nbsp;Амазон отличается в&nbsp;основном тем, что всю рутину по&nbsp;работе с&nbsp;клиентами, оформлением продаж, возвратов и&nbsp;т.&nbsp;д. берет на&nbsp;себя сам Амазон, так как в&nbsp;этой модели вы&nbsp;отправляете товары на&nbsp;склады Амазон, с&nbsp;которых и&nbsp;происходит эта автоматическая торговля. Звучит очень здорово, не&nbsp;правда&nbsp;ли?</p>\n<p>Амазон возьмет с&nbsp;вас деньги за&nbsp;хранение товара, за&nbsp;обработку заказов&nbsp;&mdash; в&nbsp;общем, за&nbsp;все&nbsp;то, что вы&nbsp;не&nbsp;будете делать самостоятельно.</p>\n<p>В&nbsp;целом, онлайн арбитраж на&nbsp;Амазон&nbsp;&mdash; это вид бизнеса, который позволяет оборачивать достаточно крупные суммы и&nbsp;получать прибыль почти мгновенно с&nbsp;момента старта бизнеса. Если у&nbsp;вас есть оборотный капитал, который хочет быть приумноженным&nbsp;&mdash; этот бизнес ваш\nМы&nbsp;поможем вам овладеть всей информацией для успешного старта вашего бизнеса.</p>\n<p>Начинать этот бизнес можно с&nbsp;суммы: $1,000</p>\n<p>Предпочтительная сумма на&nbsp;старте: $2,000&nbsp;&mdash; $3,000</p>\n<p>Сроки выхода на&nbsp;окупаемость: 2-4 месяца</p>\n<p>Потенциальная прибыль через год: $40,000-50,000&nbsp;в месяц</p>\n<p>Маржинальность: 8-30%</p>\n              ",
                            b: "\n<p>Личное приглашение&nbsp;7% от&nbsp;цены проданного продукта</p>\n<p>Командный бонус&nbsp;5% от&nbsp;меньшей цены из&nbsp;сформированной пары</p>\n              "
                        }
                    }, {
                        name: "personal_consultations",
                        label: "Персональные консультации",
                        price: "250 $",
                        sidebar_table: [{label: "Время консультации:", value: "60 минут"}],
                        description: {a: "\n<p>Персональные консультации&nbsp;&mdash; 250$ цена за&nbsp;консультацию</p>\n<p>Время консультации 60&nbsp;минут</p>\n<p>Разбираются любые вопросы по&nbsp;торговле на&nbsp;amazon.com</p>\n              "}
                    }]
                }
            }, methods: {
                goto: function (n, t) {
                    t && (n = this.current_index + n), n < 0 && (n = this.products.length - 1), n + 1 > this.products.length && (n = 0), this.current_index = n
                }
            }
        }, r = p, i = s("2877"), b = Object(i["a"])(r, e, a, !1, null, null, null);
        t["a"] = b.exports
    },
    // bd0c: function (n, t, s) {
    //     "use strict";
    //     var e = function () {
    //             var n = this, t = n.$createElement, s = n._self._c || t;
    //             return s("div", [s("Header"), n._t("default"), s("Footer")], 2)
    //         }, a = [], p = function () {
    //             var n = this, t = n.$createElement, s = n._self._c || t;
    //             return s("header", {staticClass: "b-header"}, [s("div", {staticClass: "b-header__inner"}, [s("div", {staticClass: "b-header__menu"}, [s("button", {
    //                 staticClass: "b-header__item",
    //                 attrs: {type: "button"},
    //                 on: {click: n.login}
    //             }, [n._v("\n        логин\n      ")]), s("button", {
    //                 staticClass: "b-header__item",
    //                 attrs: {type: "button"},
    //                 on: {click: n.register}
    //             }, [n._v("\n        регистрация\n      ")]), s("router-link", {
    //                 staticClass: "b-header__item",
    //                 attrs: {to: {name: "partnership"}}
    //             }, [n._v("\n        сотрудничество\n      ")])], 1), s("div", {staticClass: "b-header__menu"}, [s("router-link", {
    //                 staticClass: "b-header__item",
    //                 attrs: {to: {name: "catalog"}}
    //             }, [n._v("\n        каталог\n      ")]), s("button", {
    //                 staticClass: "b-header__item _lang",
    //                 attrs: {type: "button"}
    //             }, [n._v("\n        dfdfd\n      ")])], 1), s("router-link", {
    //                 staticClass: "b-header__logo",
    //                 attrs: {to: {name: "home"}}
    //             })], 1)])
    //         }, r = [], i = {
    //             methods: {
    //                 login: function () {
    //                     this.$modal.show("login")
    //                 }, register: function () {
    //                     this.$modal.show("register")
    //                 }
    //             }
    //         }, b = i, o = s("2877"), l = Object(o["a"])(b, p, r, !1, null, null, null), c = l.exports, _ = function () {
    //             var n = this, t = n.$createElement;
    //             n._self._c;
    //             return n._m(0)
    //         }, u = [function () {
    //             var n = this, t = n.$createElement, s = n._self._c || t;
    //             return s("footer", {staticClass: "b-footer"}, [s("div", {staticClass: "b-footer__inner"}, [n._v("\n    [Footer]\n  ")])])
    //         }], d = {}, m = Object(o["a"])(d, _, u, !1, null, null, null), v = m.exports,
    //         h = {components: {Header: c, Footer: v}}, f = h, C = Object(o["a"])(f, e, a, !1, null, null, null);
    //     t["a"] = C.exports
    // }
});
//# sourceMappingURL=app.47b15c52.js.map