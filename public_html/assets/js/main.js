$(function () {

    // lang toggle
    $('.lang-btn').on('click', function () {
        $('.lang-list').toggle();
    });

    // sliding line
    function switchUnderline(switchBlock) {
        var underline = $(switchBlock).find('.underline'),
            active = $(switchBlock).find('.active');
        underline.css({
            width: active.width(),
            left: active.position().left
        });
    }
    if ($('.switch-block').length !== 0) {
        switchUnderline('.switch-block');
    }

    $('.switch-btn').on('click', function (e) {
        e.preventDefault();
        $('.switch-btn').removeClass('active');
        $(this).addClass('active');
        switchUnderline('.switch-block');
    });

    // hide/show on focus/blur text on input

    $('#who-invited').on('focus', function () {
        $(this).next().hide();
    }).on('blur', function () {
        if ($(this).val() === '') {
            $(this).next().show();
        }
    });


    // send password
    $('.restore-pass').on('click', function (e) {
        e.preventDefault();
        $('.enter-form form input').each(function () {
            if ($(this).val() === '' || $(this).val() === null) {
                $(this).addClass('error');
            } else {
                $(this).removeClass('error');
            }
        });
        if ($('.enter-form form .error').length === 0) {
            $('.toggle:not(".sended")').hide();
            $('.toggle.sended').show();
            $('.enter-form form').hide();
        }
    });



    function mobileCheck() {
        if ($(window).width() <= 768) {
            $('header .right-menu').append($('footer'));
        } else {
            $('body').append($('footer'));
        }
    }

    mobileCheck();

    $(window).on('resize', function () {
        mobileCheck();
    });


    $(document).on('click', '.toggle-nav', function () {
        $('.right-menu').fadeIn().css({'display': 'flex'});
    });
    $(document).on('click', '.right-menu .close', function () {
        $('.right-menu').fadeOut();
    });




});