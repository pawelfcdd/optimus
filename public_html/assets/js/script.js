// main page

$('.toggle-btn').on('click', function () {
    $(this).next().slideToggle();
    $(this).toggleClass('rotated');
});


$(document).on('click', '.popup .close-btn',function () {
    $('.popup-wrapper').fadeOut();
});

$(document).on('click', '.order-robot', function (e) {
    e.preventDefault();
    $('.popup-wrapper').fadeIn();
    $('.robot-name-input').val($(this).data('robot'));
    $('.popup-wrapper .robot-name').text($(this).data('robot'));
});

if ($("input[type='tel']") !== 0) {
    $("input[type='tel']").mask("+7 (999) 999-99-99");
}

$('.info-btn').hover(function () {
    $('.info-block').stop().fadeIn();
}, function () {
    $('.info-block').stop().fadeOut();
});


$(document).on('click', '.card .switch-btn',function () {
    $('.desc-slider .info-switch').hide();
    $('.' + $(this).data('toggle')).show();
});


function urlCheckRules() {
    var url = $(location).attr('href').split('#')[1];
    $('.answers').hide();
    $('.' + url).show();
    $('.left-menu a').removeClass('active');
    $('.left-menu a[href="#' + url + '"]').addClass('active');
}
if ($('.main.faq').length !== 0) {
    urlCheckRules();
}

$(document).on('click', '.left-menu a', function () {
    $('.left-menu a').removeClass('active');
    $(this).addClass('active');
    $('.answers').hide();
    $('.' + $(this).attr('href').split('#')[1]).show();
});
