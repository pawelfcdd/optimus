<?php

namespace App\Repository;

use App\Entity\AmazonProduct;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AmazonProduct|null find($id, $lockMode = null, $lockVersion = null)
 * @method AmazonProduct|null findOneBy(array $criteria, array $orderBy = null)
 * @method AmazonProduct[]    findAll()
 * @method AmazonProduct[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AmazonProductRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AmazonProduct::class);
    }

    // /**
    //  * @return AmazonProduct[] Returns an array of AmazonProduct objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AmazonProduct
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
