<?php

namespace App\Repository;

use App\Entity\ClientInformation;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ClientInformation|null find($id, $lockMode = null, $lockVersion = null)
 * @method ClientInformation|null findOneBy(array $criteria, array $orderBy = null)
 * @method ClientInformation[]    findAll()
 * @method ClientInformation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClientInformationRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ClientInformation::class);
    }


    /**
     * @param User $user
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function isClientVerified(User $user)
    {
        return $this->createQueryBuilder('c')
            ->select('c.isVerified')
            ->where('c.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getOneOrNullResult();
    }

    // /**
    //  * @return ClientInformation[] Returns an array of ClientInformation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ClientInformation
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
