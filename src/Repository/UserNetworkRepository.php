<?php

namespace App\Repository;

use App\Entity\UserNetwork;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UserNetwork|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserNetwork|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserNetwork[]    findAll()
 * @method UserNetwork[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserNetworkRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserNetwork::class);
    }

    // /**
    //  * @return UserNetwork[] Returns an array of UserNetwork objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserNetwork
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
