<?php

namespace App\Repository;


use App\Entity\User;
use App\Entity\VerificationRequest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class VerificationRequestRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, VerificationRequest::class);
    }
    /**
     * @param User $user
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findVerificationRequestByUser(User $user)
    {
        return $this->createQueryBuilder('v')
            ->select()
            ->where('v.user = :user')
            ->setParameter('user', $user->getId())
            ->getQuery()
            ->getOneOrNullResult();
    }
}