<?php

namespace App\Repository;

use App\Entity\AmazonProductSalesData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AmazonProductSalesData|null find($id, $lockMode = null, $lockVersion = null)
 * @method AmazonProductSalesData|null findOneBy(array $criteria, array $orderBy = null)
 * @method AmazonProductSalesData[]    findAll()
 * @method AmazonProductSalesData[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AmazonProductSalesDataRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AmazonProductSalesData::class);
    }

    // /**
    //  * @return AmazonProductSalesData[] Returns an array of AmazonProductSalesData objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AmazonProductSalesData
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
