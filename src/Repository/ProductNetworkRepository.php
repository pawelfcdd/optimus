<?php

namespace App\Repository;

use App\Entity\ProductNetwork;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ProductNetwork|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductNetwork|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductNetwork[]    findAll()
 * @method ProductNetwork[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductNetworkRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ProductNetwork::class);
    }

    // /**
    //  * @return ProductNetwork[] Returns an array of ProductNetwork objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProductNetwork
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
