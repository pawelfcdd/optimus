<?php

namespace App\Repository;

use App\Entity\PaymentAmount;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PaymentAmount|null find($id, $lockMode = null, $lockVersion = null)
 * @method PaymentAmount|null findOneBy(array $criteria, array $orderBy = null)
 * @method PaymentAmount[]    findAll()
 * @method PaymentAmount[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PaymentAmountRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PaymentAmount::class);
    }

    // /**
    //  * @return PaymentAmount[] Returns an array of PaymentAmount objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PaymentAmount
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
