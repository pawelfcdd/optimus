<?php

namespace App\Repository;

use App\Entity\ProductBonus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ProductBonus|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductBonus|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductBonus[]    findAll()
 * @method ProductBonus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductBonusRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ProductBonus::class);
    }

    // /**
    //  * @return ProductBonus[] Returns an array of ProductBonus objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProductBonus
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
