<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Product::class);
    }

    /**
     * @param string $tradingType
     * @return mixed
     */
    public function findByTradingType(string $tradingType)
    {
        return $this->createQueryBuilder('p')
            ->where('p.tradingType = :tradingType')
            ->setParameter('tradingType', $tradingType)
            ->orderBy('p.name', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function getImagesByTradingType(string $tradingType, string $sortOrder = 'DESC')
    {
        return $this->createQueryBuilder('p')
            ->select('p.image')
            ->where('p.tradingType = :tradingType')
            ->orderBy('p.sortOrder', $sortOrder)
            ->setParameter('tradingType', $tradingType)
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return Product[] Returns an array of Product objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Product
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
