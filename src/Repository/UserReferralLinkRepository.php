<?php

namespace App\Repository;

use App\Entity\UserReferralLink;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UserReferralLink|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserReferralLink|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserReferralLink[]    findAll()
 * @method UserReferralLink[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserReferralLinkRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserReferralLink::class);
    }

    // /**
    //  * @return UserReferralLink[] Returns an array of UserReferralLink objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserReferralLink
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
