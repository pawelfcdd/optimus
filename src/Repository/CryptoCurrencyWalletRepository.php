<?php

namespace App\Repository;

use App\Entity\CryptoCurrencyWallet;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CryptoCurrencyWallet|null find($id, $lockMode = null, $lockVersion = null)
 * @method CryptoCurrencyWallet|null findOneBy(array $criteria, array $orderBy = null)
 * @method CryptoCurrencyWallet[]    findAll()
 * @method CryptoCurrencyWallet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CryptoCurrencyWalletRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CryptoCurrencyWallet::class);
    }

    // /**
    //  * @return CryptoCurrencyWallet[] Returns an array of CryptoCurrencyWallet objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CryptoCurrencyWallet
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
