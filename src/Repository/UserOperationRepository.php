<?php

namespace App\Repository;

use App\Entity\UserOperation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UserOperation|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserOperation|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserOperation[]    findAll()
 * @method UserOperation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserOperationRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserOperation::class);
    }

    // /**
    //  * @return UserOperation[] Returns an array of UserOperation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserOperation
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
