<?php

namespace App\AdvCash;

use SoapClient;

class SendMoney extends AdvCashController
{
    /**
     * @var float
     */
    private $amount;

    /**
     * @var string
     */
    private $currency;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $note;

    /** @var string  */
    private $wsdl = 'https://wallet.advcash.com:8443/wsm/merchantWebService?wsdl';

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     * @return SendMoney
     */
    public function setAmount(float $amount): SendMoney
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     * @return SendMoney
     */
    public function setCurrency(string $currency): SendMoney
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return SendMoney
     */
    public function setEmail(string $email): SendMoney
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getNote(): string
    {
        return $this->note;
    }

    /**
     * @param string $note
     * @return SendMoney
     */
    public function setNote(string $note): SendMoney
    {
        $this->note = $note;
        return $this;
    }

    private final function getAuthToken(string $secretWord)
    {
        $gmt = gmdate('Ymd:H');
        $token = hash("sha256", $secretWord . ':' . $gmt);
        return $token;
    }




    /**
     * @throws \Exception
     */
    function init($secretWord)
    {
        $token = $this->getAuthToken($secretWord);
        $soapClient = new \SoapClient($this->wsdl); 

        try {
            $validParameters = array(
                "(getBalances)",
            );
            $args = func_get_args();
            $this->_checkArguments($args, $validParameters);

            $getBalancesResponse = $soapClient->__soapCall("getBalances", $args);

            echo print_r($getBalancesResponse, true)."<br/><br/>";
            echo print_r($getBalancesResponse->return, true)."<br/><br/>";
        } catch (Exception $e) {
            echo "ERROR MESSAGE => " . $e->getMessage() . "<br/>";
            echo $e->getTraceAsString();
        }

    }
}