<?php


namespace App\Service;


class Mailer
{
    /** @var \Swift_Message */
    private $message;

    public function createMessage(string $subject, string $setFrom, string $setTo, $body)
    {
        $this->message = (new \Swift_Message($subject))
            ->setFrom($setFrom)
            ->setTo($setTo)
            ->setBody($body, 'text/html');

        return $this;
    }

    public function sendMail(\Swift_Mailer $mailer)
    {
        $mailer->send($this->message);

        return $this;
    }
}