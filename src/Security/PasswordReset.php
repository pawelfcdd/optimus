<?php


namespace App\Security;


use App\Controller\Front\FrontController;
use App\Entity\ResetPasswordRequest;
use App\Entity\User;
use App\Form\ResetPasswordRequestType;
use App\Service\Mailer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class PasswordReset extends FrontController
{
    /**
     * @Route("/reset-password", name="app.client.reset_password")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function resetPasswordRequestForm(Request $request, \Swift_Mailer $swift_Mailer)
    {
        if ($request->isMethod('post')) {
            $userEmail = $request->request->get('email');
            $user = $this->em->getRepository(User::class)->findOneBy(['email' => $userEmail]);

            if ($user) {
                $resetPasswordRequest = new ResetPasswordRequest();
                $resetPasswordRequest
                    ->setUser($user)
                    ->setIsUsed(false)
                    ->setToken($this->generateToken())
                ;

                $this->em->persist($resetPasswordRequest);
                $this->em->flush();

                $resetPasswordToken = $resetPasswordRequest->getToken();

                $mailer = new Mailer();
                $mailer->createMessage(
                    'Запроос на восстановление пароля',
                    'noreply@optimusale.ru',
                    $userEmail,
                    'Ссылка для восстановления пароля - ' . $request->getScheme() . '://' . $request->getHttpHost() . $this->generateUrl('app.client.reset_password_page', [
                        'token' => $resetPasswordToken,
                    ]))
                    ->sendMail($swift_Mailer);

                $this->addFlash('success', 'На ваш email отправлена инструкция с восстановленем пароля');
            } else {
                $this->addFlash('error', 'Пользователь с электронной почтой ' . $userEmail. ' не обнаружен');
            }
        }

        return $this->render('front/layouts/security/reset_password_request_form.html.twig', []);
    }


    public function resetPasswordForm($token, Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $resetPasswordRequest = $this->em->getRepository(ResetPasswordRequest::class)->findOneBy([
            'token' => $token,
            'isUsed' => false
        ]);

        if ($resetPasswordRequest) {
            $parameters['invalide_token'] = false;
            if ($request->isMethod('post')) {
                $userPassword = $request->request->get('password');
                $userPasswordRepeat = $request->request->get('password_repeat');

                $userPassword = trim($userPassword);
                $userPasswordRepeat = trim($userPasswordRepeat);

                if ($userPassword === $userPasswordRepeat) {
                    $user = $resetPasswordRequest->getUser();
                    $user->setPassword($passwordEncoder->encodePassword($user, $userPassword));
                    $resetPasswordRequest->setIsUsed(true);
                    $this->em->flush();
                    $this->addFlash('success', 'Пароль успешно изменен!');
                    $parameters['invalide_token'] = true;
                    $parameters['message'] = 'Пароль сохранен';

                } else {
                    $this->addFlash('error', 'Пароли не совпадают!');
                }
            }
        } else {
            $parameters['invalide_token'] = true;
            $parameters['message'] = 'Эта ссылка уже была использована';
        }

        return $this->render('front/layouts/security/reset_password_form.html.twig', $parameters);
    }

    private function generateToken()
    {
        return rtrim(strtr(base64_encode(random_bytes(32)), '+/', '-_'), '=');
    }
}