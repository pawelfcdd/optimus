<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\UserReferralLink;
use App\Form\RegistrationFormType;
use App\Security\ClientAuthenticator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;

class RegistrationController extends AbstractController
{
    /** @var EntityManagerInterface  */
    private $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @Route("/register", name="app_register")
     *
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param GuardAuthenticatorHandler $guardHandler
     * @param ClientAuthenticator $authenticator
     * @param \Swift_Mailer $mailer
     *
     * @return Response
     */
    public function register(
        Request $request,
        UserPasswordEncoderInterface $passwordEncoder,
        GuardAuthenticatorHandler $guardHandler,
        ClientAuthenticator $authenticator,
        \Swift_Mailer $mailer): Response
    {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {

            $referralLink = $request->query->get('referral_code');

            $referralLink = $this->getDoctrine()->getRepository(UserReferralLink::class)
                ->findOneBy([
                    'referralCode' => $referralLink
                ]);

            if ($referralLink) {
                $this->createUser($passwordEncoder, $form, $referralLink);
            } else {
                $this->createUser($passwordEncoder, $form);
            }

            // do anything else you need here, like send an email

            $message = (new \Swift_Message('Спасибо за регистрацию'))
                ->setFrom('info@optimusale.com')
                ->setTo($user->getEmail())
                ->setBody($this->renderView('email/registration.html.twig', ['name' => $user->getEmail()]), 'text/html');

            $mailer->send($message);

            return $guardHandler->authenticateUserAndHandleSuccess(
                $user,
                $request,
                $authenticator,
                'main' // firewall name in security.yaml
            );
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }

    private function createUser(UserPasswordEncoderInterface $passwordEncoder, FormInterface $form, UserReferralLink $referralLink = null)
    {
        $form->getData()->setPassword(
            $passwordEncoder->encodePassword(
                $form->getData(),
                $form->get('plainPassword')->getData()
            )
        );
        $form->getData()->setRoles(['ROLE_CLIENT']);

        $this->em->persist($form->getData());
        $this->em->flush();

        if ($referralLink) {
            $referralLink->addInvitedUser($form->getData());
            $this->em->flush();
        }

    }
}
