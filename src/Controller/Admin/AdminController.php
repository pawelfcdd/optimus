<?php

namespace App\Controller\Admin;


use App\Entity\ClientInformation;
use App\Entity\Issue;
use App\Entity\Product;
use App\Entity\ProductBonus;
use App\Entity\UserOperation;
use App\Entity\VerificationRequest;
use App\Form\ProductBonusType;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AdminController
 * @package App\Controller\Admin
 */
class AdminController extends EasyAdminController
{
    public $em;
    /** @var array The full configuration of the entire backend */
    protected $config;

    /** @var array The full configuration of the current entity */
    protected $entity;

    /** @var Request The instance of the current Symfony request */
    protected $request;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/verify-user", name="app_admin_verify_user")
     * @param Request $request
     * @return Response
     */
    public function verifyUser(Request $request)
    {
        $verificationRequestId = $request->query->get('id');
        /** @var VerificationRequest $verificationRequest */
        $verificationRequest = $this->em->getRepository(VerificationRequest::class)->findOneBy(['id' => $verificationRequestId]);
        /** @var ClientInformation $userInformation */
        $userInformation = $this->em->getRepository(ClientInformation::class)->findOneBy(['user' => $verificationRequest->getUser()]);

        return $this->render('admin/verify_page.html.twig', [
            'userInformation' => $userInformation,
            'image' => $this->getParameter('app.path.verification_images') . '/' . $verificationRequest->getImage(),
            'user_id' => $verificationRequest->getUser(),
            'verificationRequestId' => $verificationRequestId,
        ]);
    }

    /**
     * @Route("/verify-user/submit/{user}", name="app_admin_verify_user_submit")
     *
     * @param Request $request
     * @param int $user
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function saveVerifyStatus(Request $request, int $user)
    {
        $verifyRequestStatus =  (int)$request->query->get('verify-request-status');
        $verificationRequestId = (int)$request->query->get('verificationRequestId');
        /** @var ClientInformation $userInformation */
        $userInformation = $this->em->getRepository(ClientInformation::class)->findOneBy(['user' => $user]);
        /** @var VerificationRequest $verificationRequest */
        $verificationRequest = $this->em->getRepository(VerificationRequest::class)->findOneBy(['id' => $verificationRequestId]);

        $userInformation->setIsVerified($verifyRequestStatus);
        $this->em->remove($verificationRequest);
        $this->em->flush();

        return $this->redirectToRoute('easyadmin');
    }

    /**
     * @Route("/user-issue", name="app_admin_user_issue")
     */
    public function userIssue(Request $request)
    {
        $userIssue = $this->em->getRepository(Issue::class)->findOneBy([
            'id' => $request->query->get('id'),
        ]);
        return $this->render('admin/issue_answer_form.html.twig', [
            'userIssue' => $userIssue,
            'image' => $this->getParameter('app.path.issues_images') . '/' . $userIssue->getImage(),
        ]);
    }

    /**
     * @Route("/user-issue/submit", name="app_admin_user_issue_submit", methods={"POST"})
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @throws \Exception
     */
    public function submitUserIssue(Request $request)
    {
        if ($request->isMethod('post')) {
            $issueAnswer = $request->request->get('issue-answer');
            $issueId = $request->request->get('issue-id');
            /** @var Issue $issue */
            $issue = $this->em->getRepository(Issue::class)->findOneBy(['id' => $issueId]);
            $issue->setIssueAnswer($issueAnswer);
            $this->em->persist($issue);
            $this->em->flush();
            return $this->redirectToRoute('easyadmin', [
                'entity' => new Issue(),
                'action' => 'list'
            ]);
        }

        throw new \Exception('Method not allowed');
    }

    /**
     * @Route("/transfer-request", name="app_admin_transfer_request")
     * @param Request $request
     * @return Response
     */
    public function transferRequest(Request $request)
    {
        /** @var UserOperation $userOperation */
        $userOperation = $this->em->getRepository(UserOperation::class)->findOneBy(['id' => $request->query->get('id')]);
        $transferStatus= $request->request->get('transfer_status');

        if ($request->isMethod('post')) {
            if ($transferStatus) {
                $totalAmount = $userOperation->getAmountToTransfer();
                $userAccountBalance = $userOperation->getUserAccount()->getBalance();

                if ($userAccountBalance >= $totalAmount) {
                    $userOperation->getUserAccount()->setBalance($userAccountBalance - $totalAmount);
                    $userOperation->setAmountAfterOperation($userOperation->getUserAccount()->getActualAmount());
                }

            }
            $userOperation->setRequestStatus($request->request->get('transfer_status'));
            $this->em->flush();
        }

        return $this->render('admin/transfer_request.html.twig', [
            'transfer' => $userOperation,
        ]);
    }

    /**
     * @Route("/product-bonus", name="app_admin_product_bonus")
     */
    public function productBonus(Request $request)
    {
        $productId = $request->query->get('id');
        /** @var Product $product */
        $product = $this->em->getRepository(Product::class)
            ->findOneBy(['id' => $productId]);
        /** @var ProductBonus $productBonus */
        $productBonus = $this->em->getRepository(ProductBonus::class)
            ->findOneBy(['product' => $product]);

        $form = $this
            ->createForm(ProductBonusType::class, $productBonus, [])
            ->handleRequest($request);

        if ($request->isMethod('POST')) {
            if ($form->isSubmitted() && $form->isValid()) {
                /** @var ProductBonus $formData */
                $formData = $form->getData();
                $formData->setProduct($product);
                $this->em->persist($formData);
                $this->em->flush();
            }
        }

        return $this->render('admin/product_bonus_form.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
