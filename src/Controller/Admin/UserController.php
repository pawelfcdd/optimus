<?php

namespace App\Controller\Admin;


use App\Entity\AmazonProduct;
use App\Entity\User;
use App\Form\AmazonProductType;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends EasyAdminController
{
    protected $em;
    protected $request;

    public function __construct(EntityManagerInterface $em, RequestStack $request)
    {
        $this->em = $em;
        $this->request = $request;
    }

    /**
     * @Route("/admin/user/{id}", name="app.admin.user_form")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction()
    {
        $currentRequest = $this->request;
        $userId = $currentRequest->get('id');
        $user = $this->em->getRepository(User::class)->findOneBy(['id' => $userId]);
        $userPackages = $user->getUserProducts();

        $userFullName = $user->getUserInformation() ? $user->getUserInformation()->getFullName() : 'Информация не предоставлена';
        $userVerification = $user->getUserInformation() ? ($user->getUserInformation()->getIsVerified() ? 'Верифицирован' : 'Не верифицирован') : 'Информация не предоставлена';

        $userAccounts = $user->getUserAccounts();
        $referralUsers = null;

        if ($user->getReferralLink()) {
            $referralUsers = $user->getReferralLink()->getInvitedUsers();
        }

        return $this->render('admin/client_page.html.twig', [
            'user' => $user,
            'userFullName' => $userFullName,
            'userVerification' => $userVerification,
            'userPackages' => $userPackages,
            'amazonProducts' => $user->getAmazonProducts(),
            'userAccounts' => $userAccounts,
            'invitedUsers' => $referralUsers,
        ]);
    }

    /**
     * @Route("/admin/user/{id}/add-amazon-product", name="app.admin.user_add_amazon_product")
     */
    public function addAmazonProductToUser(User $user)
    {

        return $this->redirectToRoute('easyadmin', [
            'action' => 'new',
            'userId' => $user->getId(),
            'entity' => 'AmazonProduct',
        ]);
    }

    /**
     * @Route("/admin/user/amazon-product/{id}/sales-data", name="app.admin.user_amazon_product_sales_data")
     */
    public function amazonProductData(AmazonProduct $amazonProduct)
    {
        $productSalesData = $amazonProduct->getSalesData();

        return $this->render('admin/client_amazon_product_sales_data.html.twig', [
            'productSalesData' => $productSalesData,
        ]);
    }
}
