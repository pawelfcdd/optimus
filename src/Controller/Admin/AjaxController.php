<?php


namespace App\Controller\Admin;


use App\Entity\UserNetworkBinaryTree;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AjaxController extends AdminController
{
    /**
     * @Route("/admin/user-referral-network/remove-user-from-network", name="app.admin.user_referral_network.remove_user_from_network")
     * @param Request $request
     */
    public function removeUserFromNetwork(Request $request)
    {
        if ($request->isMethod('post')) {
            $userInNetwork = $this->em->getRepository(UserNetworkBinaryTree::class)->findOneBy([
                'treeMember' => $request->request->get('nodeId'),
                'userNetwork' => $request->request->get('networkid'),
            ]);


            $this->em->remove($userInNetwork);
            $this->em->flush();

            $pathToRedirect = $this->generateUrl('app.admin.user_referral_network', [
                'user' => $request->request->get('networkownerid')
            ]);
            return JsonResponse::create($pathToRedirect, 200);

        }
    }
}