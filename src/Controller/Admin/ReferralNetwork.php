<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Entity\UserNetwork;
use App\Entity\UserNetworkBinaryTree;
use App\Entity\UserReferralLink;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ReferralNetwork extends AdminController
{
    /**
     * @Route("/admin/user-referral-network/add-user-to-network", name="app.admin.add_user_to_referral_network", methods={"POST"})
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function addUserToNetwork(Request $request)
    {
        if ($request->isMethod('POST'))
        {
            $requestPostData = $request->request->all();
            $treeMemberUser = $this->em->getRepository(User::class)->findOneBy([
                'id' => $requestPostData['tree_member_id']
            ]);
            $invitedByUser = $this->em->getRepository(User::class)->findOneBy([
                'id' => $requestPostData['invited_by_id']
            ]);
            $userNetowrk = $this->em->getRepository(UserNetwork::class)->findOneBy([
                'id' => $requestPostData['user_network_id']
            ]);

            if ($treeMemberUser && $invitedByUser && $userNetowrk) {
                $userNetworkBinaryTree = new UserNetworkBinaryTree();
                $userNetworkBinaryTree
                    ->setUserNetwork($userNetowrk)
                    ->setTreeBranch($userNetowrk->getActiveBranch())
                    ->setTreeMember($treeMemberUser)
                    ->setInvitedBy($invitedByUser)
                    ->setUserIdBefore($requestPostData['user_id_before'])
                ;

                $this->em->persist($userNetworkBinaryTree);
                $this->em->flush();
                return $this->redirectToRoute('app.admin.user_referral_network', [
                    'user' => $invitedByUser->getId()
                ]);
            }
        };
    }

    /**
     * @Route("/admin/user-referral-network/{user}", name="app.admin.user_referral_network")
     */
    public function showNetwork(User $user)
    {
        $userNetwork = $this->em->getRepository(UserNetwork::class)->findOneBy([
            'user' => $user
        ]);

        if (!$userNetwork) {
            return $this->redirectToRoute('app.admin.user_form', [
                'id' => $user->getId()
            ]);
        }

        $userNetworkBinaryTree = $this->em->getRepository(UserNetworkBinaryTree::class)
            ->findBy([
                'userNetwork' => $userNetwork
            ]);

        $referralLink = $this->em->getRepository(UserReferralLink::class)->findOneBy(['userId' => $user]);

        $invitedUsers = $referralLink->getInvitedUsers()->toArray();

        $invitedUsersInNetwork = $this->em->getRepository(UserNetworkBinaryTree::class)
            ->findBy([
                'treeMember' => $invitedUsers
            ]);


        $invitedUsersMap = [];
        $invitedUsersInNetworkMap = [];

        if (empty($invitedUsersInNetwork)) {
            $notInNetworkUsers = $invitedUsers;
        } else {

            foreach ($invitedUsers as $invitedUser) {
                $invitedUsersMap[] = $invitedUser->getId();
            }


            foreach ($invitedUsersInNetwork as $userInNetwork) {
                $invitedUsersInNetworkMap[] = $userInNetwork->getTreeMember()->getId();
            }
            $notInNetworkUsers = array_diff($invitedUsersMap, $invitedUsersInNetworkMap);
        }

        $notInNetworkUsers = $this->em->getRepository(User::class)->findBy(['id' => $notInNetworkUsers]);

        return $this->render('admin/binary_network/show_network.html.twig', [
            'user' => $user,
            'userNetwork' => $userNetwork,
            'userNetworkBinaryTree' => $userNetworkBinaryTree,
            'invites' => $user->getInvites(),
            'notInNetworkUsers' => $notInNetworkUsers,
            'invitedUsersInNetwork' => $invitedUsersInNetwork,
        ]);
    }

}