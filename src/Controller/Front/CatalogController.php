<?php

namespace App\Controller\Front;

class CatalogController extends FrontController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function catalogPage()
    {
        $products = $this->getProductsOrderedByTradingType();
        $optimusAiProducts = $this->sortProductsByTradingType($products, 'optimus_ai');
        $onlineEducationProducts = $this->sortProductsByTradingType($products, 'online_education');
        $optimusAiProductsCount = count($optimusAiProducts);

        return $this->render('front/layouts/catalog.html.twig', [
            'products' => $products,
            'firstProductInList' => $this->getFirstProductFromList($products),
            'productName' => $this->getFirstProductFromList($products)->getName(),
            'optimusAiProductsCount' => $optimusAiProductsCount,
            'optimusAiProducts' => array_reverse($optimusAiProducts),
            'onlineEducationProducts' => array_reverse($onlineEducationProducts),
        ]);
    }
}