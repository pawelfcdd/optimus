<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 08.07.19
 * Time: 16:10
 */

namespace App\Controller\Front;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class FormsController extends AbstractController
{

    private $mailer;

    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function sendMessage(Request $request)
    {
        $formData = $request->query->all();

        if (!empty($formData)) {

            $subject = null;

            switch ($formData['modal_type']) {
                case 'buy_request':
                    $subject = 'Запрос на покупку';
                    break;
                case 'consultation':
                    $subject = 'Запрос на консультацию';
                    break;
            }


            $message = (new \Swift_Message($subject))
                ->setFrom('info@optimusale.com')
                ->setTo(['info@optimusale.com', 'paulnovikov@ukr.net'])
                ->setBody($this->renderView('email/buy_request.html.twig', ['formData' => $formData]), 'text/html');

            $this->mailer->send($message);

            return $this->redirectToRoute('app.front.index');
        }
    }
}
