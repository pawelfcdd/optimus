<?php

namespace App\Controller\Front;

use App\Form\RegistrationFormType;

class IndexController extends FrontController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexPage()
    {
        $products = $this->getProductsOrderedByTradingType();
        $optimusAiProducts = $this->sortProductsByTradingType($products, 'optimus_ai');
        $onlineEducationProducts = $this->sortProductsByTradingType($products, 'online_education');
        $optimusAiProductsCount = count($optimusAiProducts);
        $registrationForm = $this->createForm(RegistrationFormType::class);

        return $this->render('front/layouts/index.html.twig', [
            'products' => $products,
            'optimusAiProductsCount' => $optimusAiProductsCount,
            'optimusAiProducts' => array_reverse($optimusAiProducts),
            'onlineEducationProducts' => array_reverse($onlineEducationProducts),
            'registrationForm' => $registrationForm->createView(),
        ]);
    }
}
