<?php

namespace App\Controller\Front;


use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class FrontController extends AbstractController
{
    /** @var EntityManagerInterface  */
    public $em;
    /** @var \App\Repository\ProductRepository|\Doctrine\Common\Persistence\ObjectRepository  */
    public $productRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
        $this->productRepository = $this->em->getRepository(Product::class);
    }


    /**
     * @param array $products
     * @return Product
     */
    protected function getFirstProductFromList(array $products)
    {
        $reversedProducts = array_reverse($products);
        /** @var Product $firstProduct */
        $firstProduct = end($reversedProducts);

        return $firstProduct;
    }

    protected function sortProductsByTradingType(array $products, string $tradingType)
    {
        $filteredProducts = [];

        foreach ($products as $product) {
            if ($product->getTradingType() == $tradingType) {
                $filteredProducts[] = $product;
            }
        }

        return $filteredProducts;
    }

    /**
     * @return array
     */
    protected function getProductsOrderedByTradingType()
    {
        $products = $this->productRepository->findBy([], [
            'tradingType' => 'DESC'
        ]);

        return $products;
    }
}