<?php

namespace App\Controller\Front;

class PartnershipController extends FrontController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function partnershipPage()
    {
        return $this->render('front/layouts/partnership.html.twig', [

        ]);
    }
}