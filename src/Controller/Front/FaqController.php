<?php

namespace App\Controller\Front;

use App\Entity\Faq;

class FaqController extends FrontController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function faqPage()
    {
        $faqs = $this->em->getRepository(Faq::class)->findAll();
        return $this->render('front/layouts/faq.html.twig', [
            'faqs' => $faqs,
        ]);
    }
}