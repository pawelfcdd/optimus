<?php

namespace App\Controller\Client;

use App\Entity\TransferMethod;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AjaxController extends ClientController
{
    /**
     * @Route("/ajax/load-transfer-method-form", name="app.client.ajax.load_transfer_method_form")
     */
    public function loadTransferMethodForm(Request $request)
    {
        $transferType = $request->query->get('transferType');
        $template = 'client/partials/transfer_method/'.$transferType.'.html.twig';
        $transferMethod = $this->em->getRepository(TransferMethod::class)->findOneBy(['type' => $transferType]);

        return $this->render($template, [
            'onLoadTransferMin' => $transferMethod->getMinimumAmount(),
            'onLoadTransferMax' => $transferMethod->getMaximumAmount(),
        ]);
    }
}