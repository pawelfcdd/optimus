<?php

namespace App\Controller\Client;
use App\Entity\News;
use Symfony\Component\Routing\Annotation\Route;

/**
 *
 * Class NewsController
 * @package App\Controller\Client
 */
class NewsController extends ClientController
{
    /**
     * @Route("/client/news/list", name="app.client.news.list")
     */
    public function newsList()
    {
        $newsList = $this->em->getRepository(News::class)->findAll();

        return $this->render('client/layouts/news/list.html.twig', [
            'pageTitle' => 'Новости',
            'newsList' => $newsList
        ]);
    }

    /**
     * @Route("/client/news/article/{slug}", name="app.client.news.article")
     *
     * @param News $article
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function articlePage(News $article)
    {
        return $this->render('client/layouts/news/single_article.html.twig', [
            'article' => $article,
        ]);
    }
}