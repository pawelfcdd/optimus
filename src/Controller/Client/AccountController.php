<?php

namespace App\Controller\Client;

use App\Entity\Account;
use App\Entity\TransferMethod;
use App\Entity\UserAccount;
use App\Entity\UserOperation;
use App\Form\UserOperationType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/client/account")
 *
 * Class AccountController
 * @package App\Controller\Client
 */
class AccountController extends ClientController
{
    /**
     * @Route("/list", name="app.client.account.list")
     */
    public function accountList()
    {
        /** @var Account $userAccounts */
        $userAccounts = $this->em->getRepository(Account::class)->getAccountWithInfoByUser($this->getCurrentUser());

        return $this->render('client/layouts/account/list.html.twig', [
            'pageTitle' => 'Список счетов',
            'userAccounts' => $userAccounts,
        ]);
    }

    /**
     * @Route("/withdrawal", name="app.client.account.withdrawal")
     */
    public function withdrawal(Request $request)
    {

        $userAccounts = $this->em
            ->getRepository(Account::class)
            ->getAccountWithInfoByUser($this->getCurrentUser());

        $transferMethods = $this->em
            ->getRepository(TransferMethod::class)
            ->findBy(['isActive' => true]);

        return $this->render('client/layouts/account/withdrawal.html.twig', [
            'pageTitle' => 'Заявка на вывод средств',
            'userAccounts' => $userAccounts,
            'transferMethods' => $transferMethods,
        ]);
    }

    /**
     * @Route("/withdrawal/request-list", name="app.client.account.withdrawal_list")
     */
    public function withdrawalRequestList()
    {
        $operations = $this->em->getRepository(UserOperation::class)->findBy(['user' => $this->getCurrentUser()]);

        return $this->render('client/layouts/account/withdrawal_request_list.html.twig', [
            'pageTitle' => 'Мои заявки',
            'withdrawalRequests' => $operations,
        ]);
    }

    /**
     * @Route("/withdrawal/submit", name="app.client.withdrawal.submit")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function withdrawalRequestHandler(Request $request)
    {
        if ($request->isMethod('post')) {
            /** @var UserAccount $account */
            $userAccount = $this->em
                ->getRepository(UserAccount::class)
                ->findOneBy([
                    'user' => $this->getCurrentUser(),
                    'account' => $request->request->get('account'),
                ]);
            /** @var TransferMethod $transferMethod */
            $transferMethod = $this->em->getRepository(TransferMethod::class)->findOneById($request->request->get('transferMethod'));
            $transferMethodType = $transferMethod->getType();

            $requestedAmount = $request->request->get('amount_withdrawal');
            $fee = ($transferMethod->getTransactionFee() / 100) * $requestedAmount;

            $userOperation = new UserOperation();

            $transactionDetails = [];

            foreach ($request->request as $key => $value) {
                if (strpos($key, $transferMethodType) === 0) {
                    $transactionDetails[$key] = $value;
                }
            }

            $userOperation
                ->setTransferMethod($transferMethod)
                ->setUser($this->getCurrentUser())
                ->setRequestedAmount($requestedAmount)
                ->setBlockedAmount($fee)
                ->setTransferDetails(json_encode($transactionDetails))
                ->setUserAccount($userAccount)
                ->setAmountBeforeOperation($userAccount->getActualAmount())
                ->setAmountAfterOperation()
            ;

            $this->em->persist($userOperation);
            $this->em->flush();

            return $this->redirectToRoute('app.client.account.withdrawal_list');
        }
    }
}