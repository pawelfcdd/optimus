<?php

namespace App\Controller\Client;

use App\Entity\ClientInformation;
use App\Entity\User;
use App\Entity\VerificationRequest;
use App\Form\ClientInformationType;
use App\Form\VerificationRequestType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class PersonalInformationController
 * @package App\Controller\Client
 */
class PersonalInformationController extends ClientController
{
    /** @var UserPasswordEncoderInterface  */
    private $passwordEncoder;

    public function __construct(EntityManagerInterface $em, UserPasswordEncoderInterface $passwordEncoder)
    {
        parent::__construct($em);

        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @Route("/client/personal-information/common-info", name="app.client.personal_info.common_info")
     */
    public function commonInformation()
    {
        /** @var User $user */
        $user = $this->getUser();
        $userInformation = $user->getUserInformation();

        return $this->render('client/layouts/presonal_information/common_information.html.twig', [
            'pageTitle' => 'Общая информация',
            'userInformation' => $userInformation,
        ]);
    }

    /**
     * @Route("/client/personal-information/verification", name="app.client.personal_info.verification")
     */
    public function verificationPage()
    {
        /** @var User $user */
        $user = $this->getUser();
        $isVerified = $this->em->getRepository(ClientInformation::class)->isClientVerified($user);
        $userInformation = $user->getUserInformation();
        $options = [
            'action' => $this->generateUrl('app.client.personal_info.verification/submit')
        ];

        $verificationRequest = $this->em->getRepository(VerificationRequest::class)->findVerificationRequestByUser($user);

        $form = $this->getVerificationForm($options);

        return $this->render('client/layouts/presonal_information/verification.html.twig', [
            'pageTitle' => 'Верификация',
            'isVerified' => $isVerified['isVerified'],
            'userInformation' => $userInformation,
            'userId' => $user->getId(),
            'form' => $form->createView(),
            'verificationRequest' => $verificationRequest,
        ]);
    }

    /**
     * @Route("/client/personal-information/change-password", name="app.client.personal_info.change_password")
     */
    public function changePassword()
    {
        return $this->render('client/layouts/presonal_information/change_password.html.twig', [
            'pageTitle' => 'Сменить пароль'
        ]);
    }

    /**
     * @Route(
     *     "/client/personal-information/change-password-action",
     *     name="app.client.personal_info.change_password_action",
     *     methods={"POST"}
     * )
     *
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function changePasswordActionHandler(Request $request)
    {
        $currentPassword = $request->request->get('current-password');

        if ($this->passwordEncoder->isPasswordValid($this->getCurrentUser(), $currentPassword)) {
            $newPassword = $request->request->get('new-password');
            $encodedPassword = $this->passwordEncoder->encodePassword($this->getCurrentUser(), $newPassword);
            $this->getCurrentUser()->setPassword($encodedPassword);
            $this->em->flush();
            $this->addFlash('success', 'Пароль был успешно сменен');

        } else {
            $this->addFlash('error', 'Неыверный пароль пользователя');
        }
        return $this->redirectToRoute('app.client.personal_info.change_password');
    }

    /**
     * @Route("/client/personal-information/security", name="app.client.personal_info.security")
     */
    public function securityPage(){}

    /**
     * @Route("/client/personal-information/login-history", name="app.client.personal_info.login_history")
     */
    public function loginHistoryPage(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        $userLogins = $user->getLoginHistories();

        return $this->render('client/layouts/presonal_information/login_history.html.twig', [
            'pageTitle' => 'История входов',
            'userLogins' => $userLogins,
        ]);
    }

    /**
     * @Route("/client/personal-information/verification/submit", name="app.client.personal_info.verification/submit", methods={"POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function saveVerificationRequest(Request $request)
    {
        $form = $this->getVerificationForm([], null, $request);

        if ($request->isMethod('post')) {
            /** @var VerificationRequest $formData */
            $formData = $form->getData();

            $formData->setImage('');

            if ($form->isSubmitted() && $form->isValid()) {

                $this->em->persist($form->getData());
                $this->em->flush();

                return $this->redirectToRoute('app.client.personal_info.verification');
            }
        }
    }

    /**
     * @Route("/client/personal-information/edit-info", name="app.client.personal_info.edit_info")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editInformation(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        $userInformation = $user->getUserInformation();
        $form = $this->createForm(ClientInformationType::class, $userInformation)->handleRequest($request);

        if ($request->isMethod('POST')) {
            if ($form->isSubmitted() && $form->isValid()) {
                /** @var ClientInformation $formData */
                $formData = $form->getData();

                $formData->setUser($user);

                $this->em->persist($form->getData());
                $this->em->flush();

                return $this->redirectToRoute('app.client.personal_info.common_info');
            }
        }

        return $this->render('client/layouts/presonal_information/common_information.html.twig', [
            'pageTitle' => 'Редактирова информацию',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param array $options
     * @param array|null $data
     * @param Request|null $request
     * @return \Symfony\Component\Form\FormInterface
     */
    private function getVerificationForm(array $options, array $data = null, Request $request = null)
    {
        $options['user_id'] = $this->getUser()->getId();
        $form = $this->createForm(VerificationRequestType::class, $data, $options);

        if ($request) {
            $form->handleRequest($request);
        }

        return $form;
    }

}