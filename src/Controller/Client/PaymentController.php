<?php

namespace App\Controller\Client;

use App\AdvCash\SendMoney;
use App\Entity\CryptoCurrencyWallet;
use App\Entity\PaymentMethod;
use App\Entity\Product;
use App\Entity\ProductNetwork;
use App\Entity\User;
use App\Entity\UserInvite;
use App\Entity\UserNetwork;
use App\Entity\UserNetworkBinaryTree;
use App\Entity\UserProduct;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 *
 * Class PaymentController
 * @package App\Controller\Client
 */
class PaymentController extends ClientController
{
    /** @var string  */
    public const CRYPTO_CURRENCY_PREFIX = 'crypto_currency';
    public const PAYMENT_METHODS_PREFIX = 'payment_method';

    /** @var array  */
    public const PREFIX_ENTITY_MAP = [
        self::CRYPTO_CURRENCY_PREFIX => CryptoCurrencyWallet::class,
        self::PAYMENT_METHODS_PREFIX => PaymentMethod::class,
    ];

    /** @var array  */
    private $paymentMethods = [];

    /**
     * @Route("/client/payment/buy-product/{product}/select-payment-type", name="app.client.payment.select_payment_type")
     *
     * @param Product $product
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function paymentPage(Product $product, Request $request)
    {
        $this->setPaymentMethods();

        return $this->render('client/layouts/payment/select_payment_type.html.twig', [
            'pageTitle' => 'Выберите способ оплаты',
            'paymentMethods' => $this->paymentMethods,
            'product' => $product
        ]);
    }


    /**
     * @Route("/client/payment/buy-product/{product}/{payment_type}", name="app.client.payment.options_list")
     */
    public function paymentOptionsList(Product $product, string $payment_type)
    {
        $paymentFQN = self::PREFIX_ENTITY_MAP[$payment_type];

        $optionsList = $this->em->getRepository($paymentFQN)->findBy([
            'isEnabled' => true
        ]);

        return $this->render('client/layouts/payment/select_payment_type.html.twig', [
            'optionsList' => $optionsList,
            'pageTitle' => 'Выберите способ оплаты',
            'payment_type' => $payment_type,
            'product' => $product,
            'activeCurrency' => $this->getActiveCurrency(),
        ]);
    }

    /**
     * @Route("/client/payment/starting-packages/submit-payment/{product}/{payment_type}",
     *     name="app.client.marketing.starting_packages.submit_payment")
     */
    public function submitPayment(Product $product, Request $request, string $payment_type)
    {
        if ($request->isMethod('post')) {
            $this->createUserProduct($product, $request, $payment_type);

            $this->em->flush();

            return $this->redirectToRoute('app.client.marketing.starting_packages.submit_payment_confirm');
        }
    }

    /**
     * @Route("/client/payment/starting-packages/submit-payment/confirmation",
     *     name="app.client.marketing.starting_packages.submit_payment_confirm")
     */
    public function submitPaymentPage()
    {
        return $this->render('client/layouts/payment/submit_payment.html.twig', [
            'pageTitle' => 'Информация о платеже'
        ]);
    }

    /**
     * @return $this
     */
    private function setPaymentMethods()
    {
        $cryptoCurrencyWallets = $this->em->getRepository(CryptoCurrencyWallet::class)->findBy([
            'isEnabled' => true
        ]);

        $paymentMethods = $this->em->getRepository(PaymentMethod::class)->findBy([
            'isEnabled' => true
        ]);

        if (!empty($cryptoCurrencyWallets)) {
            $this->paymentMethods[self::CRYPTO_CURRENCY_PREFIX]['label'] = 'Оплатить с помощью крипто валют';
        }

        if (!empty($paymentMethods)) {
            $this->paymentMethods[self::PAYMENT_METHODS_PREFIX]['label'] = 'Выберите другие способы оплаты';
        }

        return $this;
    }

    /**
     * @param Product $product
     * @param Request $request
     * @param $payment_type
     * @return $this
     */
    private function createUserProduct(Product $product, Request $request, $payment_type)
    {
        /** @var UserProduct $userProduct */
        $userProduct = new UserProduct();

        $userProduct
            ->setUser($this->getCurrentUser())
            ->setProduct($product)
            ->setAmountPaid($request->request->get('amount-to-pay'))
            ->setPaymentEntityFQN(self::PREFIX_ENTITY_MAP[$payment_type])
            ->setPaymentType($payment_type)
            ->setIsApproved(false);


        $this->em->persist($userProduct);

        return $this;
    }

    /**
     * @param Product $product
     * @return $this
     */
    private function assignUserToNetwork(Product $product)
    {
        $userInvite = $this->em->getRepository(UserInvite::class)
            ->findOneBy(['userInvited' => $this->getCurrentUser()]);

        if ($userInvite) {
            $networkToJoin = $this->em->getRepository(UserNetwork::class)
                ->findOneBy(['user' => $userInvite->getUser()]);

            $userNetworkBinaryTree = new UserNetworkBinaryTree();
            $productNetwork = new ProductNetwork();

            $userNetworkBinaryTree
                ->setInvitedBy($userInvite->getUser())
                ->setTreeBranch($networkToJoin->getActiveBranch())
                ->setTreeMember($this->getCurrentUser())
                ->setUserNetwork($networkToJoin);

            $productNetwork
                ->setProduct($product)
                ->setNetwork($networkToJoin);

            $this->em->persist($userNetworkBinaryTree);
            $this->em->persist($productNetwork);

        } else {
            $userNetwork = $this->em->getRepository(UserNetwork::class)
                ->findOneBy([
                    'user' => $this->getCurrentUser()
                ]);

            if (!$userNetwork) {
                $userNetwork = new UserNetwork();
                $userNetwork->setUser($this->getCurrentUser());
                $this->em->persist($userNetwork);

                $this->em->flush();
            }

            $productNetwork = new ProductNetwork();
            $productNetwork
                ->setNetwork($userNetwork)
                ->setProduct($product);

            $this->em->persist($productNetwork);
        }

        return $this;
    }
}