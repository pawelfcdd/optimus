<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 14.06.19
 * Time: 15:13
 */

namespace App\Controller\Client;


use App\Entity\Issue;
use App\Entity\User;
use App\Form\IssueType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 *
 * Class HelpController
 * @package App\Controller\Client
 */
class HelpController extends ClientController
{
    /**
     * @Route("/client/help/create-issue", name="app.client.help.create_issue")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createIssue()
    {
        $options = [
            'action' => $this->generateUrl('app.client.help.submit_issue'),
        ];
        $form = $this->getIssueForm($options);
        return $this->render('client/layouts/help/create_issue.html.twig', [
            'pageTitle' => 'Новый запрос',
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/client/help/create-issue/submit", name="app.client.help.submit_issue")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function saveIssue(Request $request)
    {
        if ($request->isMethod('post')) {
            $form = $this->getIssueForm([], $request);
            /** @var Issue $formData */
            $formData = $form->getData();
            $formData->setUser($this->getUser());
            $this->em->persist($formData);
            $this->em->flush();

            return $this->redirectToRoute('app.client.help.create_issue');
        }
    }

    /**
     * @Route("/client/help/my-issues", name="app.client.help.my_issues")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listIssues()
    {
        /** @var User $user */
        $user = $this->getUser();
        $issues = $user->getIssues();

        return $this->render('client/layouts/help/issue_list.html.twig', [
            'pageTitle' => 'Мои запросы',
            'issues' => $issues,
        ]);
    }

    /**
     * @Route("/client/help/my-issues/issue/{id}", name="app.client.help.view_issue")
     */
    public function viewIssue(Issue $issue)
    {
        return $this->render('client/layouts/help/view_issue.html.twig', [
            'pageTitle' => 'Детали запроса',
            'issue' => $issue,
        ]);
    }

    /**
     * @param array $options
     * @param Request|null $request
     * @return \Symfony\Component\Form\FormInterface
     */
    private function getIssueForm(array $options = [], Request $request = null)
    {
        $form = $this->createForm(IssueType::class, null, $options);

        if ($request) {
            $form->handleRequest($request);
        }

        return $form;
    }
}