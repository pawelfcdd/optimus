<?php

namespace App\Controller\Client;

use App\Entity\Currency;
use App\Entity\Product;
use App\Entity\User;
use App\Entity\UserInvite;
use App\Entity\UserNetwork;
use App\Entity\UserNetworkBinaryTree;
use App\Entity\UserProduct;
use App\Form\BinaryTreeSwitchBranchType;
use App\Form\UserNetworkType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 *
 * Class MarketingController
 * @package App\Controller\Client
 */
class MarketingController extends ClientController
{
    const INVITES_LIMIT = 2;

    /**
     * @Route("/client/marketing/starting-packages", name="app.client.marketing.starting_packages")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function startingPackages()
    {
        $products = $this->em->getRepository(Product::class)->findAll();
        $userPackages = $this->em->getRepository(UserProduct::class)->findBy(['user' => $this->getCurrentUser()]);

        return $this->render('client/layouts/marketing/starting_packages.html.twig', [
            'pageTitle' => 'Стартовые пакеты',
            'products' => $products,
            'userPackages' => $userPackages,
        ]);
    }

    /**
     * @Route("/client/marketing/referral-link", name="app.client.marketing.referral_link")
     */
    public function referralLink()
    {
        $referralLink = $this->getCurrentUser()->getReferralLink();

        return $this->render('client/layouts/marketing/referral_link.html.twig', [
            'pageTitle' => 'Реферальная ссылка',
            'referralLink' => $referralLink,
        ]);
    }

    /**
     * @Route("/client/marketing/starting-packages/buy/{package}", name="app.client.marketing.starting_packages.buy")
     * @param Product $package
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function buyPackage(Product $package, Request $request)
    {
        if ($request->isMethod('POST')) {
            $userProduct = new UserProduct();
            $userProduct
                ->setUser($this->getCurrentUser())
                ->setProduct($package)
                ->setExpiredAt(new \DateTime());

            $this->em->persist($userProduct);
            $this->em->flush();

            $userInvite = $this->em->getRepository(UserInvite::class)
                ->findOneBy(['userInvited' => $this->getCurrentUser()]);

            if ($userInvite) {
                if (!$userInvite->getIsProductBuy()) {
                    $userInvite->setIsProductBuy(true);
                    $this->em->persist($userInvite);
                }

                $networkToJoin = $this->em->getRepository(UserNetwork::class)
                    ->findOneBy(['user' => $userInvite->getUser()]);

                $userNetworkBinaryTree = new UserNetworkBinaryTree();

                $userNetworkBinaryTree
                    ->setInvitedBy($userInvite->getUser())
                    ->setTreeBranch($networkToJoin->getActiveBranch())
                    ->setTreeMember($this->getCurrentUser())
                    ->setUserNetwork($networkToJoin);

                $this->em->persist($userNetworkBinaryTree);
                $this->em->flush();
            }
        }

        $activeCurrency = $this->em->getRepository(Currency::class)->findOneBy(['isActive' => true]);

        return $this->render('client/layouts/marketing/buy_package.html.twig', [
            'pageTitle' => 'Купить пакет',
            'package' => $package,
            'activeCurrency' => $activeCurrency
        ]);
    }

    /**
     * @Route("/client/marketing/starting-packages/info/{package}", name="app.client.marketing.starting_packages.info")
     */
    public function productInfo(Product $package, Request $request)
    {
        $invites = $this->em->getRepository(UserInvite::class)
            ->findBy([
                'user' => $this->getCurrentUser(),
                'product' => $package
            ]);


        if ($request->isMethod('post')) {
            $userInvite = new UserInvite();
            $userInvite
                ->setProduct($package)
                ->setUser($this->getCurrentUser())
                ->setInviteCode($this->generateInviteCode());

            $this->em->persist($userInvite);
            $this->em->flush();

            return $this->redirectToRoute('app.client.marketing.starting_packages.info', [
                'package' => $package->getId()
            ]);
        }

        return $this->render('client/layouts/marketing/product_info.html.twig', [
            'pageTitle' => $package->getName() . ' info',
            'package' => $package,
            'invites' => $invites,
        ]);
    }

    /**
     * @Route("/client/marketing/remove-invite/{userInvite}/{package}", name="app.client.marketing.remove_invite")
     */
    public function removeInviteCodeAction(UserInvite $userInvite, Product $package, Request $request)
    {
        if ($request->isMethod('POST')) {
            $this->em->remove($userInvite);
            $this->em->flush();
        }

        return $this->redirectToRoute('app.client.marketing.starting_packages.info', [
            'package' => $package->getId()
        ]);

    }

    /**
     * @Route("/client/marketing/binary-network-list", name="app.client.marketing.binary_network_list")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function binaryNetworkList()
    {
        $participantUserNetworks = [];
        $userNetworks = $this->em->getRepository(UserNetwork::class)
            ->findBy(['user' => $this->getCurrentUser()]);

//        if (!$userNetworks) {
//            $userNetworks = [];
//            $userInvite = $this->em->getRepository(UserInvite::class)->findOneBy(['userInvited' => $this->getCurrentUser()]);
//            $getUserInvitedId = $userInvite->getUser()->getId();
//            $participantUserNetworks = $this->em->getRepository(UserNetwork::class)
//                ->findBy(['user' => $getUserInvitedId]);
//        }

        return $this->render('client/layouts/marketing/binary.html.twig', [
            'pageTitle' => 'Ваши бинарные структуры',
            'userNetworks' => $userNetworks,
            'participantUserNetworks' => $participantUserNetworks,
        ]);
    }

    /**
     * @Route("/client/marketing/edit-network/{userNetwork}", name="app.client.marketing.edit_network")
     * @param UserNetwork $userNetwork
     * @param Request $request
     * @return Response
     */
    public function editNetwork(UserNetwork $userNetwork, Request $request)
    {
        $form = $this->createForm(UserNetworkType::class, $userNetwork, [])->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();
            $this->addFlash('success', 'Настройки сети были успешно изменены');
            return $this->redirectToRoute('app.client.marketing.binary_network_list');
        }

        return $this->render('client/layouts/marketing/binary.html.twig', [
            'pageTitle' => 'Редактировать данные сети',
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/client/marketing/binary-network", name="app.client.marketing.binary_network")
     *
     * @return  Response
     */
    public function binaryNetwork()
    {

        $userNetwork = $this->em->getRepository(UserNetwork::class)->findOneBy([
            'user' => $this->getCurrentUser()
        ]);

        $userNetworkBinaryTree = $this->em->getRepository(UserNetworkBinaryTree::class)
            ->findBy([
                'userNetwork' => $userNetwork
            ]);

        $branchChoiceForm = $this->createForm(BinaryTreeSwitchBranchType::class, $userNetwork);
        $formOptions = [
            'attr' => [
                'class' => 'form-inline'
            ],
            'action' => $this->generateUrl('app.client.marketing.binary_network.change_active_branch'),
        ];

        $branchChoiceForm = $this->createSwitchBranchForm($formOptions, $userNetwork);

        return $this->render('client/layouts/marketing/binary.html.twig', [
            'pageTitle' => 'Бинарная структура',
            'userNetwork' => $userNetwork,
            'userNetworkBinaryTree' => $userNetworkBinaryTree,
            'notInNetworkUsers' => null,
            'form' => $branchChoiceForm->createView(),
        ]);
    }

    /**
     * @Route("/client/marketing/binary-network/change-active-branch",
     *     name="app.client.marketing.binary_network.change_active_branch",
     *     methods={"POST"})
     * @param Request $request
     */
    public function changeBinaryTreeActiveBranch(Request $request)
    {
        $userNetwork = $this->em->getRepository(UserNetwork::class)->findOneBy([
            'user' => $this->getCurrentUser()
        ]);
        $form = $this->createSwitchBranchForm([], $userNetwork)->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();
            return $this->redirectToRoute('app.client.marketing.binary_network');
        }
    }

    /**
     * @param array $options
     * @param null $data
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createSwitchBranchForm($options = [], $data = null)
    {
        $form = $this->createForm(BinaryTreeSwitchBranchType::class, $data, $options);

        return $form;
    }
}
