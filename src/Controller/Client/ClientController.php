<?php

namespace App\Controller\Client;


use App\Entity\AmazonProduct;
use App\Entity\Currency;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 *
 * Class ClientController
 * @package App\Controller\Client
 */
class ClientController extends AbstractController
{
    /** @var EntityManagerInterface $em */
    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/client", name="app.client.index")
     */
    public function indexPage()
    {
        $currentUserAmazonProducts = $this->em->getRepository(AmazonProduct::class)->findBy([
            'user' => $this->getCurrentUser()
        ]);

        $referralLink = $this->getCurrentUser()->getReferralLink();

        return $this->render('client/layouts/index.html.twig', [
            'pageTitle' => 'Главная',
            'amazonProducts' => $currentUserAmazonProducts,
            'referralLink' => $referralLink
        ]);
    }

    /**
     * @return User
     */
    public function getCurrentUser()
    {
        return $this->getUser();
    }

    public function generateInviteCode($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * @return Currency|object|null
     */
    public function getActiveCurrency()
    {
        return $this->em->getRepository(Currency::class)->findOneBy(['isActive' => true]);
    }
}
