<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190619184017 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user_account (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, account_id INT NOT NULL, balance DOUBLE PRECISION NOT NULL, blocked DOUBLE PRECISION NOT NULL, UNIQUE INDEX UNIQ_253B48AEA76ED395 (user_id), INDEX IDX_253B48AE9B6B5FBA (account_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_operation (id INT AUTO_INCREMENT NOT NULL, transfer_method_id INT DEFAULT NULL, user_id INT NOT NULL, user_account_id INT NOT NULL, request_date DATETIME NOT NULL, request_status TINYINT(1) DEFAULT NULL, requested_amount DOUBLE PRECISION NOT NULL, blocked_amount DOUBLE PRECISION NOT NULL, transfer_details VARCHAR(1000) NOT NULL, UNIQUE INDEX UNIQ_19612B89B40E000C (transfer_method_id), INDEX IDX_19612B89A76ED395 (user_id), INDEX IDX_19612B893C0C9956 (user_account_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_account ADD CONSTRAINT FK_253B48AEA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_account ADD CONSTRAINT FK_253B48AE9B6B5FBA FOREIGN KEY (account_id) REFERENCES account (id)');
        $this->addSql('ALTER TABLE user_operation ADD CONSTRAINT FK_19612B89B40E000C FOREIGN KEY (transfer_method_id) REFERENCES transfer_method (id)');
        $this->addSql('ALTER TABLE user_operation ADD CONSTRAINT FK_19612B89A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_operation ADD CONSTRAINT FK_19612B893C0C9956 FOREIGN KEY (user_account_id) REFERENCES user_account (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_operation DROP FOREIGN KEY FK_19612B893C0C9956');
        $this->addSql('DROP TABLE user_account');
        $this->addSql('DROP TABLE user_operation');
    }
}
