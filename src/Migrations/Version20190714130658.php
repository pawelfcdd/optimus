<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190714130658 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user_referral_link (id INT AUTO_INCREMENT NOT NULL, user_id_id INT NOT NULL, referral_code VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_CB7041739D86650F (user_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_referral_link ADD CONSTRAINT FK_CB7041739D86650F FOREIGN KEY (user_id_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user ADD user_referral_link_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D64989911C8A FOREIGN KEY (user_referral_link_id) REFERENCES user_referral_link (id)');
        $this->addSql('CREATE INDEX IDX_8D93D64989911C8A ON user (user_referral_link_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D64989911C8A');
        $this->addSql('DROP TABLE user_referral_link');
        $this->addSql('DROP INDEX IDX_8D93D64989911C8A ON user');
        $this->addSql('ALTER TABLE user DROP user_referral_link_id');
    }
}
