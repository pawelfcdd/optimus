<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190611223342 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE client_information (id INT AUTO_INCREMENT NOT NULL, full_name VARCHAR(255) NOT NULL, is_verified TINYINT(1) NOT NULL, registration_date DATETIME NOT NULL, phone_number VARCHAR(255) DEFAULT NULL, country VARCHAR(255) NOT NULL, region VARCHAR(255) NOT NULL, city VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE client_information_user (client_information_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_88F25AAFF33738E (client_information_id), INDEX IDX_88F25AAFA76ED395 (user_id), PRIMARY KEY(client_information_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE client_information_user ADD CONSTRAINT FK_88F25AAFF33738E FOREIGN KEY (client_information_id) REFERENCES client_information (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE client_information_user ADD CONSTRAINT FK_88F25AAFA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE client_information_user DROP FOREIGN KEY FK_88F25AAFF33738E');
        $this->addSql('DROP TABLE client_information');
        $this->addSql('DROP TABLE client_information_user');
    }
}
