<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190618214830 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE transfer_method (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, minimum_amount DOUBLE PRECISION DEFAULT NULL, maximum_amount DOUBLE PRECISION DEFAULT NULL, transaction_fee INT DEFAULT NULL, type VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_operation ADD transfer_method_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_operation ADD CONSTRAINT FK_19612B89B40E000C FOREIGN KEY (transfer_method_id) REFERENCES transfer_method (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_19612B89B40E000C ON user_operation (transfer_method_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_operation DROP FOREIGN KEY FK_19612B89B40E000C');
        $this->addSql('DROP TABLE transfer_method');
        $this->addSql('DROP INDEX UNIQ_19612B89B40E000C ON user_operation');
        $this->addSql('ALTER TABLE user_operation DROP transfer_method_id');
    }
}
