<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190618231551 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE user_operation_user');
        $this->addSql('ALTER TABLE user_operation ADD user_id INT NOT NULL, ADD transfer_details VARCHAR(1000) NOT NULL');
        $this->addSql('ALTER TABLE user_operation ADD CONSTRAINT FK_19612B89A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_19612B89A76ED395 ON user_operation (user_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user_operation_user (user_operation_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_C9665481C4604981 (user_operation_id), INDEX IDX_C9665481A76ED395 (user_id), PRIMARY KEY(user_operation_id, user_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE user_operation_user ADD CONSTRAINT FK_C9665481A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_operation_user ADD CONSTRAINT FK_C9665481C4604981 FOREIGN KEY (user_operation_id) REFERENCES user_operation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_operation DROP FOREIGN KEY FK_19612B89A76ED395');
        $this->addSql('DROP INDEX IDX_19612B89A76ED395 ON user_operation');
        $this->addSql('ALTER TABLE user_operation DROP user_id, DROP transfer_details');
    }
}
