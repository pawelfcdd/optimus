<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190709191124 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE amazone_product');
        $this->addSql('ALTER TABLE amazon_product_sales_data ADD amazon_product_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE amazon_product_sales_data ADD CONSTRAINT FK_E507F0556B92C248 FOREIGN KEY (amazon_product_id) REFERENCES amazon_product (id)');
        $this->addSql('CREATE INDEX IDX_E507F0556B92C248 ON amazon_product_sales_data (amazon_product_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE amazone_product (id INT AUTO_INCREMENT NOT NULL, buy_product TINYINT(1) NOT NULL, production DATETIME NOT NULL, tracking_code VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, date_of_sale DATETIME DEFAULT NULL, number_of_sale VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, income VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, information LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci, referral_link VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, image VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, created_at DATETIME NOT NULL, title VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE amazon_product_sales_data DROP FOREIGN KEY FK_E507F0556B92C248');
        $this->addSql('DROP INDEX IDX_E507F0556B92C248 ON amazon_product_sales_data');
        $this->addSql('ALTER TABLE amazon_product_sales_data DROP amazon_product_id');
    }
}
