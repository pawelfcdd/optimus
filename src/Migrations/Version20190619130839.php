<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190619130839 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user_operation_transfer_method (user_operation_id INT NOT NULL, transfer_method_id INT NOT NULL, INDEX IDX_B3663998C4604981 (user_operation_id), INDEX IDX_B3663998B40E000C (transfer_method_id), PRIMARY KEY(user_operation_id, transfer_method_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_operation_transfer_method ADD CONSTRAINT FK_B3663998C4604981 FOREIGN KEY (user_operation_id) REFERENCES user_operation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_operation_transfer_method ADD CONSTRAINT FK_B3663998B40E000C FOREIGN KEY (transfer_method_id) REFERENCES transfer_method (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE user_operation_user');
        $this->addSql('ALTER TABLE user_operation DROP FOREIGN KEY FK_19612B89B40E000C');
        $this->addSql('DROP INDEX UNIQ_19612B89B40E000C ON user_operation');
        $this->addSql('ALTER TABLE user_operation DROP transfer_method_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user_operation_user (user_operation_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_C9665481A76ED395 (user_id), INDEX IDX_C9665481C4604981 (user_operation_id), PRIMARY KEY(user_operation_id, user_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE user_operation_user ADD CONSTRAINT FK_C9665481A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_operation_user ADD CONSTRAINT FK_C9665481C4604981 FOREIGN KEY (user_operation_id) REFERENCES user_operation (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE user_operation_transfer_method');
        $this->addSql('ALTER TABLE user_operation ADD transfer_method_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_operation ADD CONSTRAINT FK_19612B89B40E000C FOREIGN KEY (transfer_method_id) REFERENCES transfer_method (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_19612B89B40E000C ON user_operation (transfer_method_id)');
    }
}
