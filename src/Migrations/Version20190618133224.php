<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190618133224 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user_account (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, account_id INT NOT NULL, balance DOUBLE PRECISION NOT NULL, blocked DOUBLE PRECISION NOT NULL, UNIQUE INDEX UNIQ_253B48AEA76ED395 (user_id), INDEX IDX_253B48AE9B6B5FBA (account_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE account (id INT AUTO_INCREMENT NOT NULL, currency_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, is_default TINYINT(1) NOT NULL, limit_per_day DOUBLE PRECISION NOT NULL, limit_per_month DOUBLE PRECISION NOT NULL, UNIQUE INDEX UNIQ_7D3656A438248176 (currency_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_operation (id INT AUTO_INCREMENT NOT NULL, request_date DATETIME NOT NULL, request_status TINYINT(1) NOT NULL, requested_amount DOUBLE PRECISION NOT NULL, blocked_amount DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_operation_account (user_operation_id INT NOT NULL, account_id INT NOT NULL, INDEX IDX_26E86D42C4604981 (user_operation_id), INDEX IDX_26E86D429B6B5FBA (account_id), PRIMARY KEY(user_operation_id, account_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_operation_user (user_operation_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_C9665481C4604981 (user_operation_id), INDEX IDX_C9665481A76ED395 (user_id), PRIMARY KEY(user_operation_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE currency (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, sign VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_account ADD CONSTRAINT FK_253B48AEA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_account ADD CONSTRAINT FK_253B48AE9B6B5FBA FOREIGN KEY (account_id) REFERENCES account (id)');
        $this->addSql('ALTER TABLE account ADD CONSTRAINT FK_7D3656A438248176 FOREIGN KEY (currency_id) REFERENCES currency (id)');
        $this->addSql('ALTER TABLE user_operation_account ADD CONSTRAINT FK_26E86D42C4604981 FOREIGN KEY (user_operation_id) REFERENCES user_operation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_operation_account ADD CONSTRAINT FK_26E86D429B6B5FBA FOREIGN KEY (account_id) REFERENCES account (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_operation_user ADD CONSTRAINT FK_C9665481C4604981 FOREIGN KEY (user_operation_id) REFERENCES user_operation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_operation_user ADD CONSTRAINT FK_C9665481A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_account DROP FOREIGN KEY FK_253B48AE9B6B5FBA');
        $this->addSql('ALTER TABLE user_operation_account DROP FOREIGN KEY FK_26E86D429B6B5FBA');
        $this->addSql('ALTER TABLE user_operation_account DROP FOREIGN KEY FK_26E86D42C4604981');
        $this->addSql('ALTER TABLE user_operation_user DROP FOREIGN KEY FK_C9665481C4604981');
        $this->addSql('ALTER TABLE account DROP FOREIGN KEY FK_7D3656A438248176');
        $this->addSql('DROP TABLE user_account');
        $this->addSql('DROP TABLE account');
        $this->addSql('DROP TABLE user_operation');
        $this->addSql('DROP TABLE user_operation_account');
        $this->addSql('DROP TABLE user_operation_user');
        $this->addSql('DROP TABLE currency');
    }
}
