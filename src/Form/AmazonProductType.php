<?php

namespace App\Form;

use App\Entity\AmazonProduct;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;

class AmazonProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('buyStatus')
            ->add('productionStatus')
            ->add('trackingCode')
            ->add('information')
            ->add('imageFile', VichFileType::class)
            ->add('referralLink')
            ->add('createdAt')
            ->add('title')
            ->add('user')
            ->add('save', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AmazonProduct::class,
        ]);
    }
}
