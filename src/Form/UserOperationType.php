<?php

namespace App\Form;

use App\Entity\Account;
use App\Entity\Currency;
use App\Entity\TransferMethod;
use App\Entity\UserAccount;
use App\Entity\UserOperation;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserOperationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('account', EntityType::class, [
                'label' => 'Выберите счет',
                'class' => Account::class,
                'query_builder' => function (EntityRepository $entityRepository) use ($options) {
                    return $entityRepository->createQueryBuilder('a')
                        ->leftJoin(UserAccount::class, 'ua', Join::WITH, 'a.id = ua.account')
                        ->leftJoin(Currency::class, 'c', Join::WITH, 'a.currency = c.id')
                        ->where('ua.user = :user')
                        ->setParameter('user', $options['user_id'])
                        ;
                },
                'choice_label' => 'name'
            ])
            ->add('transferMethod', EntityType::class, [
                'label' => 'Метод вывода',
                'class' => TransferMethod::class,
                'choice_label' => 'name',
                'expanded' => true,
                'multiple' => false
            ])
            ->add('requestedAmount')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserOperation::class,
        ]);

        $resolver->setRequired('user_id');
    }
}
