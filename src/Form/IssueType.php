<?php

namespace App\Form;

use App\Entity\Issue;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class IssueType extends AbstractType
{
    public const SECTION_CHOICES = [
        'technical' => 'Технические вопросы',
        'verification' => 'Вопросы с верификацией',
        'financial' => 'Вопросы по финансам',
        'referral' => 'Вопросы по реферальной программе',
        'products' => 'Вопросы по продуктам',
    ];

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('section', ChoiceType::class, [
                'label' => 'Раздел',
                'choices' => array_flip(self::SECTION_CHOICES)
            ])
            ->add('title', TextType::class, [
                'label' => 'Тема',
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Описание',
            ])
            ->add('imageFile', FileType::class, [
                'label' => 'Файл',
                'required' => false,
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Отправить',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Issue::class,
        ]);
    }
}
