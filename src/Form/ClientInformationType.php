<?php

namespace App\Form;

use App\Entity\ClientInformation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClientInformationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('full_name', TextType::class, [
                'label' => 'Полное имя'
            ])
//            ->add('is_verified')
//            ->add('registration_date')
            ->add('phone_number', TextType::class, [
                'label' => 'Телефон'
            ])
            ->add('country', TextType::class, [
                'label' => 'Страна'
            ])
            ->add('region', TextType::class, [
                'label' => 'Регион'
            ])
            ->add('city', TextType::class, [
                'label' => 'Город'
            ])
//            ->add('invited_by')
            ->add('save', SubmitType::class, [
                'label' => 'Сохранить'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ClientInformation::class,
        ]);
    }
}
