<?php

namespace App\Form;

use App\Entity\UserNetwork;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BinaryTreeSwitchBranchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('activeBranch', ChoiceType::class, [
                'multiple' => false,
                'expanded' => false,
                'choices' => [
                    'Верхняя' => 'top',
                    'Нижняя' => 'bottom',
                ],
                'label' => 'Выберите активную ветку',
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Сохранить'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserNetwork::class,
        ]);
    }
}
