<?php

namespace App\Form;

use App\Entity\UserNetwork;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserNetworkType extends AbstractType
{

    public const ACTIVE_BRANCH_CHOICES = [
        'Право' => 'right',
        'Лево' => 'left',
    ];

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'required' => false,
            ])
            ->add('activeBranch', ChoiceType::class, [
                'multiple' => false,
                'expanded' => true,
                'choices' => self::ACTIVE_BRANCH_CHOICES
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserNetwork::class,
        ]);
    }
}
