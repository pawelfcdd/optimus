<?php

namespace App\Twig;

use App\Entity\AmazonProduct;
use App\Entity\AmazonProductSalesData;
use App\Entity\ClientInformation;
use App\Entity\Currency;
use App\Entity\Product;
use App\Entity\User;
use App\Entity\UserInvite;
use App\Entity\UserProduct;
use App\Repository\ClientInformationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    /** @var EntityManagerInterface  */
    private $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    public function getFilters()
    {
        return [
            new TwigFilter('get_price_with_currency_sign', [$this, 'getPriceWithCurrencySign']),
        ];
    }

    public function getPriceWithCurrencySign(string $price = null) {

        if ($price !== false) {
            /** @var Currency $currentCurrency */
            $currentCurrency = $this->em->getRepository(Currency::class)->findOneBy(['isActive' => true]);

            return $price . ' ' . $currentCurrency->getSign();
        }

        return null;
    }

    /**
     * @return array|TwigFunction[]
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('isPackageBuy', [$this, 'isPackageBuy']),
            new TwigFunction('showByProductNotification', [$this, 'showByProductNotification']),
            new TwigFunction('isPaymentApprove', [$this, 'isPaymentApprove']),
            new TwigFunction('isUserVerified', [$this, 'isUserVerified']),
            new TwigFunction('getAmazonProductDataLatest', [$this, 'getAmazonProductDataLatest']),
        ];
    }

    /**
     * @param AmazonProduct $amazonProduct
     * @return AmazonProductSalesData|null|object
     */
    public function getAmazonProductDataLatest(AmazonProduct $amazonProduct)
    {
        $latestData = $this->em->getRepository(AmazonProductSalesData::class)
            ->findOneBy(['amazonProduct' => $amazonProduct], ['dateOfSale' => 'DESC']);

        return $latestData;
    }

    /**
     * @param User $user
     * @param Product $product
     *
     * @return bool
     */
    public function isPackageBuy(User $user, Product $product)
    {
        $userProduct = $this->em->getRepository(UserProduct::class)
            ->findOneBy(['user' => $user, 'product' => $product]);

        return $userProduct ? true : false;
    }

    public function showByProductNotification(User $user)
    {
        $userInvite = $this->em->getRepository(UserInvite::class)->findOneBy([
            'userInvited' => $user
        ]);

        if (!$userInvite) {
            return false;
        }

        if (!$userInvite->getIsProductBuy()) {
            return $userInvite;
        }

        return false;
    }

    /**
     * @param User $user
     * @param Product $product
     * @return bool|null
     */
    public function isPaymentApprove(User $user, Product $product)
    {
        $userProduct = $this->em->getRepository(UserProduct::class)
            ->findOneBy([
                'user' => $user,
                'product' => $product
            ]);

        return $userProduct->getIsApproved();
    }

    public function isUserVerified(User $user)
    {
        $userInformation = $this->em->getRepository(ClientInformation::class)->findOneBy([
            'user' => $user
        ]);

        if (!$userInformation) {
            return false;
        }

        return $userInformation->getIsVerified();
    }
}