<?php

namespace App\DataFixtures;

use App\Entity\Currency;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CurrencyFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $currency = new Currency();

        $currency
            ->setName('US Dollar')
            ->setSign('$')
            ->setIsActive(1);

        $manager->persist($currency);
        $manager->flush();
    }
}