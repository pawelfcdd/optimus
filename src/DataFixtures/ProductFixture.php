<?php

namespace App\DataFixtures;

use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ProductFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $products = [
            1 => [
                'name' => 'Exper',
                'description' => 'Test',
                'price' => 3000,
                'tradingType' => 'optimus_ai'
            ],
            2 => [
                'name' => 'Middle',
                'description' => 'Test',
                'price' => 2000,
                'tradingType' => 'optimus_ai'
            ],
            3 => [
                'name' => 'Minimal',
                'description' => 'Test',
                'price' => 1000,
                'tradingType' => 'optimus_ai'
            ],
            4 => [
                'name' => 'Персональные консультации',
                'description' => 'Test',
                'price' => 3000,
                'tradingType' => 'online_education'
            ],
            5 => [
                'name' => 'Private label',
                'description' => 'Test',
                'price' => 2000,
                'tradingType' => 'online_education'
            ],
            6 => [
                'name' => 'Online арбитраж',
                'description' => 'Test',
                'price' => 1000,
                'tradingType' => 'online_education'
            ],
        ];

        foreach ($products as $key => $productItem) {
            $product =  new Product();
            $product
                ->setName($productItem['name'])
                ->setDescription($productItem['description'])
                ->setPrice($productItem['price'])
                ->setTradingType($productItem['tradingType'])
                ->setSortOrder($key)
                ->setNumberOfGoodsMin(1)
                ->setNumberOfGoodsMax(1)
            ;

            $manager->persist($product);
        }

        $manager->flush();
    }
}
