<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AdminFixture extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();

        $user
            ->setEmail('admin@mail.com')
            ->setRoles(['ROLE_SUPER_ADMIN', 'ROLE_CLIENT'])
            ->setPassword($this->passwordEncoder->encodePassword($user, '123'));

        $manager->persist($user);

        $manager->flush();
    }
}
