<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AmazonProductSalesDataRepository")
 */
class AmazonProductSalesData
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $dateOfSale;

    /**
     * @ORM\Column(type="integer")
     */
    private $numberOfSales;

    /**
     * @ORM\Column(type="float")
     */
    private $income;
    
    //TODO: add relation to Amazon product
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\AmazonProduct", inversedBy="salesData")
     * @ORM\JoinColumn(name="amazon_product_id", referencedColumnName="id")
     */
    private $amazonProduct;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateOfSale(): ?\DateTimeInterface
    {
        return $this->dateOfSale;
    }

    public function setDateOfSale(\DateTimeInterface $dateOfSale): self
    {
        $this->dateOfSale = $dateOfSale;

        return $this;
    }

    public function getNumberOfSales(): ?int
    {
        return $this->numberOfSales;
    }

    public function setNumberOfSales(int $numberOfSales): self
    {
        $this->numberOfSales = $numberOfSales;

        return $this;
    }

    public function getIncome(): ?float
    {
        return $this->income;
    }

    public function setIncome(float $income): self
    {
        $this->income = $income;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAmazonProduct()
    {
        return $this->amazonProduct;
    }

    /**
     * @param mixed $amazonProduct
     * @return AmazonProductSalesData
     */
    public function setAmazonProduct($amazonProduct)
    {
        $this->amazonProduct = $amazonProduct;
        return $this;
    }
}
