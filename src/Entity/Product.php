<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 * @Vich\Uploadable()
 * @ORM\HasLifecycleCallbacks()
 */
class Product
{
    public const TRADING_TYPES = [
        'online_education' => 'Онлайн образование',
        'optimus_ai' => 'Нейросеть Оптимус',
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     */
    private $price;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string | null
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="product_images", fileNameProperty="image")
     */
    private $imageFile;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var string
     * @ORM\Column()
     */
    private $tradingType;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\ProductBonus", mappedBy="product", cascade={"persist", "remove"})
     */
    private $productBonus;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserProduct", mappedBy="product")
     */
    private $userProducts;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserInvite", mappedBy="product")
     */
    private $userInvites;

    /**
     * @ORM\Column(type="integer")
     */
    private $sortOrder;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $numberOfGoodsMin;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $numberOfGoodsMax;

    public function __construct()
    {
        $this->userProducts = new ArrayCollection();
        $this->userInvites = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @param File|null $image
     * @return $this
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->setCreatedAt();
        }

        return $this;
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getImage()
    {
        return $this->image;
    }

    /**
     * @ORM\PrePersist()
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();

        return $this;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return string
     */
    public function getTradingType(): ?string
    {
        return $this->tradingType;
    }

    /**
     * @param string $tradingType
     * @return Product
     */
    public function setTradingType(string $tradingType): Product
    {
        $this->tradingType = $tradingType;
        return $this;
    }

    public function getProductBonus(): ?ProductBonus
    {
        return $this->productBonus;
    }

    public function setProductBonus(?ProductBonus $productBonus): self
    {
        $this->productBonus = $productBonus;

        // set (or unset) the owning side of the relation if necessary
        $newProduct = $productBonus === null ? null : $this;
        if ($newProduct !== $productBonus->getProduct()) {
            $productBonus->setProduct($newProduct);
        }

        return $this;
    }

    /**
     * @return Collection|UserProduct[]
     */
    public function getUserProducts(): Collection
    {
        return $this->userProducts;
    }

    public function addUserProduct(UserProduct $userProduct): self
    {
        if (!$this->userProducts->contains($userProduct)) {
            $this->userProducts[] = $userProduct;
            $userProduct->setProduct($this);
        }

        return $this;
    }

    public function removeUserProduct(UserProduct $userProduct): self
    {
        if ($this->userProducts->contains($userProduct)) {
            $this->userProducts->removeElement($userProduct);
            // set the owning side to null (unless already changed)
            if ($userProduct->getProduct() === $this) {
                $userProduct->setProduct(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserInvite[]
     */
    public function getUserInvites(): Collection
    {
        return $this->userInvites;
    }

    public function addUserInvite(UserInvite $userInvite): self
    {
        if (!$this->userInvites->contains($userInvite)) {
            $this->userInvites[] = $userInvite;
            $userInvite->setProduct($this);
        }

        return $this;
    }

    public function removeUserInvite(UserInvite $userInvite): self
    {
        if ($this->userInvites->contains($userInvite)) {
            $this->userInvites->removeElement($userInvite);
            // set the owning side to null (unless already changed)
            if ($userInvite->getProduct() === $this) {
                $userInvite->setProduct(null);
            }
        }

        return $this;
    }

    public function getSortOrder(): ?int
    {
        return $this->sortOrder;
    }

    public function setSortOrder(int $sortOrder): self
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNumberOfGoodsMin()
    {
        return $this->numberOfGoodsMin;
    }

    /**
     * @param mixed $numberOfGoodsMin
     * @return Product
     */
    public function setNumberOfGoodsMin(int $numberOfGoodsMin)
    {
        $this->numberOfGoodsMin = $numberOfGoodsMin;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNumberOfGoodsMax()
    {
        return $this->numberOfGoodsMax;
    }

    /**
     * @param mixed $numberOfGoodsMax
     * @return Product
     */
    public function setNumberOfGoodsMax(int $numberOfGoodsMax)
    {
        $this->numberOfGoodsMax = $numberOfGoodsMax;
        return $this;
    }


}
