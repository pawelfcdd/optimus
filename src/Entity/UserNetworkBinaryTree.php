<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserNetworkBinaryTreeRepository")
 */
class UserNetworkBinaryTree
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User")
     */
    private $treeMember;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="userNetworkBinaryTrees")
     */
    private $invitedBy;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $treeBranch;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UserNetwork", inversedBy="network")
     * @ORM\JoinColumn(nullable=false)
     */
    private $userNetwork;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $userIdBefore;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTreeMember(): ?User
    {
        return $this->treeMember;
    }

    public function setTreeMember(?User $treeMember): self
    {
        $this->treeMember = $treeMember;

        return $this;
    }

    public function getInvitedBy(): ?User
    {
        return $this->invitedBy;
    }

    public function setInvitedBy(?User $invitedBy): self
    {
        $this->invitedBy = $invitedBy;

        return $this;
    }

    public function getTreeBranch(): ?string
    {
        return $this->treeBranch;
    }

    public function setTreeBranch(string $treeBranch): self
    {
        $this->treeBranch = $treeBranch;

        return $this;
    }

    public function getUserNetwork(): ?UserNetwork
    {
        return $this->userNetwork;
    }

    public function setUserNetwork(?UserNetwork $userNetwork): self
    {
        $this->userNetwork = $userNetwork;

        return $this;
    }

    /**
     * @return int
     */
    public function getUserIdBefore(): int
    {
        return $this->userIdBefore;
    }

    /**
     * @param int $userIdBefore
     * @return UserNetworkBinaryTree
     */
    public function setUserIdBefore(int $userIdBefore): UserNetworkBinaryTree
    {
        $this->userIdBefore = $userIdBefore;
        return $this;
    }
}
