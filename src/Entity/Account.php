<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AccountRepository")
 */
class Account
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isDefault;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $limitPerDay;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $limitPerMonth;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserAccount", mappedBy="account")
     */
    private $userAccounts;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Currency")
     * @ORM\JoinColumn(name="currency_id", referencedColumnName="id")
     */
    private $currency;

    public function __toString()
    {
        return $this->getName();
    }

    public function __construct()
    {
        $this->userAccounts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIsDefault(): ?bool
    {
        return $this->isDefault;
    }

    public function setIsDefault(bool $isDefault): self
    {
        $this->isDefault = $isDefault;

        return $this;
    }

    public function getLimitPerDay(): ?float
    {
        return $this->limitPerDay;
    }

    public function setLimitPerDay(float $limitPerDay = null): self
    {
        $this->limitPerDay = $limitPerDay;

        return $this;
    }

    public function getLimitPerMonth(): ?float
    {
        return $this->limitPerMonth;
    }

    public function setLimitPerMonth(float $limitPerMonth = null): self
    {
        $this->limitPerMonth = $limitPerMonth;

        return $this;
    }

    /**
     * @return Collection|UserAccount[]
     */
    public function getUserAccounts(): Collection
    {
        return $this->userAccounts;
    }

    public function addUserAccount(UserAccount $userAccount): self
    {
        if (!$this->userAccounts->contains($userAccount)) {
            $this->userAccounts[] = $userAccount;
            $userAccount->setAccount($this);
        }

        return $this;
    }

    public function removeUserAccount(UserAccount $userAccount): self
    {
        if ($this->userAccounts->contains($userAccount)) {
            $this->userAccounts->removeElement($userAccount);
            // set the owning side to null (unless already changed)
            if ($userAccount->getAccount() === $this) {
                $userAccount->setAccount(null);
            }
        }

        return $this;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }
}
