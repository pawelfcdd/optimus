<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserAccountRepository")
 */
class UserAccount
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="userAccount")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Account", inversedBy="userAccounts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $account;

    /**
     * @ORM\Column(type="float")
     */
    private $balance;

    /**
     * @ORM\Column(type="float")
     */
    private $blocked;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserOperation", mappedBy="userAccount")
     */
    private $operations;

    public function __construct()
    {
        $this->operations = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getAccount()->getName() . ' ('  . $this->getBalance() . $this->getAccount()->getCurrency()->getSign() . ')';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getAccount(): ?Account
    {
        return $this->account;
    }

    public function setAccount(?Account $account): self
    {
        $this->account = $account;

        return $this;
    }

    public function getBalance(): ?float
    {
        return $this->balance ;
    }

    public function setBalance(float $balance): self
    {
        $this->balance = $balance;

        return $this;
    }

    public function getBlocked(): ?float
    {
        return $this->blocked;
    }

    public function setBlocked(float $blocked): self
    {
        $this->blocked = $blocked;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getOperations()
    {
        return $this->operations;
    }

    public function addOperation(UserOperation $operation): self
    {
        if (!$this->operations->contains($operation)) {
            $this->operations->add($operation);
        }

        return $this;
    }

    public function removeOperation(UserOperation $operation): self
    {
        if ($this->operations->contains($operation)) {
            $this->operations->removeElement($operation);
        }

        return $this;
    }

    public function getActualAmount() {
        return $this->getBalance() - $this->getBlocked();
    }
}
