<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserOperationRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class UserOperation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UserAccount", inversedBy="operations")
     * @ORM\JoinColumn(name="user_account_id", referencedColumnName="id")
     */
    private $userAccount;

    /**
     * @ORM\Column(type="datetime")
     */
    public $requestDate;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $requestStatus;

    /**
     * @ORM\Column(type="float")
     */
    private $requestedAmount;

    /**
     * @ORM\Column(type="float", length=255)
     */
    private $blockedAmount;

    /**
     * @ORM\Column(type="float")
     */
    private $amountBeforeOperation;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TransferMethod")
     * @ORM\JoinColumn(name="tramsfer_method_id", referencedColumnName="id")
     */
    private $transferMethod;

    /**
     * @ORM\Column(type="string", length=1000)
     */
    private $transferDetails;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="userOperations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $amountAfterOperation;

    public function __toString()
    {
        return '';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUserAccount(): ?UserAccount
    {
        return $this->userAccount;
    }

    /**
     * @param mixed $userAccount
     * @return UserOperation
     */
    public function setUserAccount($userAccount)
    {
        $this->userAccount = $userAccount;

        return $this;
    }

    public function getRequestDate(): ?\DateTimeInterface
    {
        return $this->requestDate;
    }

    /**
     * @ORM\PrePersist()
     */
    public function setRequestDate()
    {
        $this->requestDate = new \DateTime();
    }

    public function getRequestStatus(): ?bool
    {
        return $this->requestStatus;
    }

    public function setRequestStatus(bool $requestStatus = null): self
    {
        $this->requestStatus = $requestStatus;

        return $this;
    }

    public function getRequestedAmount(): ?float
    {
        return $this->requestedAmount;
    }

    public function setRequestedAmount(float $requestedAmount): self
    {
        $this->requestedAmount = $requestedAmount;

        return $this;
    }

    public function getBlockedAmount(): ?float
    {
        return $this->blockedAmount;
    }

    public function setBlockedAmount(float $blockedAmount): self
    {
        $this->blockedAmount = $blockedAmount;

        return $this;
    }

    public function getTransferMethod(): ?TransferMethod
    {
        return $this->transferMethod;
    }

    public function setTransferMethod(?TransferMethod $transferMethod): self
    {
        $this->transferMethod = $transferMethod;

        return $this;
    }

    public function getTransferDetails(): ?string
    {
        return $this->transferDetails;
    }

    public function setTransferDetails(string $transferDetails): self
    {
        $this->transferDetails = $transferDetails;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getAmountToTransfer() {
        return $this->getRequestedAmount() + $this->getBlockedAmount();
    }

    /**
     * @return mixed
     */
    public function getAmountBeforeOperation(): ?float
    {
        return $this->amountBeforeOperation;
    }

    /**
     * @param $amountBeforeOperation
     * @return UserOperation
     */
    public function setAmountBeforeOperation($amountBeforeOperation): self
    {
        $this->amountBeforeOperation = $amountBeforeOperation;

        return $this;
    }

    public function getAmountAfterOperation(): ?float
    {
        return $this->amountAfterOperation;
    }

    /**
     * @ORM\PreFlush()
     */
    public function setAmountAfterOperation(): self
    {
        $transactionFee = $this->getBlockedAmount();
        $this->amountAfterOperation = $this->getAmountBeforeOperation() - $this->getRequestedAmount() - $transactionFee;
        $this->getUserAccount()->setBlocked($this->getRequestedAmount() + $transactionFee);

        if ($this->getRequestStatus()) {
            $this->getUserAccount()->setBalance($this->amountAfterOperation);
            $this->getUserAccount()->setBlocked(0);
        }

        return $this;
    }


}
