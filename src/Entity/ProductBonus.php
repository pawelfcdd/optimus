<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductBonusRepository")
 */
class ProductBonus
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Product", inversedBy="productBonus", cascade={"persist", "remove"})
     */
    private $product;

    /**
     * @ORM\Column(type="float")
     */
    private $inviteBonus;

    /**
     * @ORM\Column(type="float")
     */
    private $teamBonus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getInviteBonus(): ?float
    {
        return $this->inviteBonus;
    }

    public function setInviteBonus(float $inviteBonus): self
    {
        $this->inviteBonus = $inviteBonus;

        return $this;
    }

    public function getTeamBonus(): ?float
    {
        return $this->teamBonus;
    }

    public function setTeamBonus(float $teamBonus): self
    {
        $this->teamBonus = $teamBonus;

        return $this;
    }
}
