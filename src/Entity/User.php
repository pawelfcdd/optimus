<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="array")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\ClientInformation", mappedBy="invitedBy")
     */
    private $invites;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\ClientInformation", mappedBy="user")
     */
    private $userInformation;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\LoginHistory", mappedBy="user")
     */
    private $loginHistories;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Issue", mappedBy="user", orphanRemoval=true)
     */
    private $issues;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserAccount", mappedBy="user")
     */
    private $userAccounts;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserOperation", mappedBy="user")
     */
    private $userOperations;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\UserNetwork", mappedBy="user", cascade={"persist", "remove"})
     */
    private $userNetwork;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserNetworkBinaryTree", mappedBy="invitedBy")
     */
    private $userNetworkBinaryTrees;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserProduct", mappedBy="user")
     */
    private $userProducts;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserInvite", mappedBy="user")
     */
    private $userInvites;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\AmazonProduct", mappedBy="user")
     */
    private $amazonProducts;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\UserReferralLink", mappedBy="userId", cascade={"persist", "remove"})
     */
    private $referralLink;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UserReferralLink", inversedBy="invitedUsers")
     */
    private $userReferralLink;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ResetPasswordRequest", mappedBy="user")
     */
    private $resetPasswordRequests;

    public function __construct()
    {
        $this->invites = new ArrayCollection();
        $this->loginHistories = new ArrayCollection();
        $this->issues = new ArrayCollection();
        $this->userOperations = new ArrayCollection();
        $this->userNetworkBinaryTrees = new ArrayCollection();
        $this->userProducts = new ArrayCollection();
        $this->userInvites = new ArrayCollection();
        $this->amazonProducts = new ArrayCollection();
        $this->userAccounts = new ArrayCollection();
        $this->resetPasswordRequests = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getEmail();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Collection|ClientInformation[]
     */
    public function getInvites(): Collection
    {
        return $this->invites;
    }

    public function addInvites(ClientInformation $clientInformation): self
    {
        if (!$this->invites->contains($clientInformation)) {
            $this->invites[] = $clientInformation;
            $clientInformation->addInvitedBy($this);
        }

        return $this;
    }

    public function removeInvites(ClientInformation $clientInformation): self
    {
        if ($this->invites->contains($clientInformation)) {
            $this->invites->removeElement($clientInformation);
            $clientInformation->removeInvitedBy($this);
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserInformation()
    {
        return $this->userInformation;
    }

    /**
     * @param mixed $userInformation
     * @return User
     */
    public function setUserInformation($userInformation)
    {
        $this->userInformation = $userInformation;
        return $this;
    }

    /**
     * @return Collection|LoginHistory[]
     */
    public function getLoginHistories(): Collection
    {
        return $this->loginHistories;
    }

    public function addLoginHistory(LoginHistory $loginHistory): self
    {
        if (!$this->loginHistories->contains($loginHistory)) {
            $this->loginHistories[] = $loginHistory;
            $loginHistory->setUser($this);
        }

        return $this;
    }

    public function removeLoginHistory(LoginHistory $loginHistory): self
    {
        if ($this->loginHistories->contains($loginHistory)) {
            $this->loginHistories->removeElement($loginHistory);
            // set the owning side to null (unless already changed)
            if ($loginHistory->getUser() === $this) {
                $loginHistory->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Issue[]
     */
    public function getIssues(): Collection
    {
        return $this->issues;
    }

    public function addIssue(Issue $issue): self
    {
        if (!$this->issues->contains($issue)) {
            $this->issues[] = $issue;
            $issue->setUser($this);
        }

        return $this;
    }

    public function removeIssue(Issue $issue): self
    {
        if ($this->issues->contains($issue)) {
            $this->issues->removeElement($issue);
            // set the owning side to null (unless already changed)
            if ($issue->getUser() === $this) {
                $issue->setUser(null);
            }
        }

        return $this;
    }

    public function getUserAccounts()
    {
        return $this->userAccounts;
    }

    public function addUserAccount(UserAccount $userAccount): self
    {
        if (!$this->userAccounts->contains($userAccount)) {
            $this->userAccounts[] = $userAccount;
            $userAccount->setAccount($this);
        }

        return $this;
    }

    public function removeUserAccount(UserAccount $userAccount): self
    {
        if ($this->userAccounts->contains($userAccount)) {
            $this->userAccounts->removeElement($userAccount);
            // set the owning side to null (unless already changed)
            if ($userAccount->getAccount() === $this) {
                $userAccount->setAccount(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserOperation[]
     */
    public function getUserOperations(): Collection
    {
        return $this->userOperations;
    }

    public function addUserOperation(UserOperation $userOperation): self
    {
        if (!$this->userOperations->contains($userOperation)) {
            $this->userOperations[] = $userOperation;
            $userOperation->setUser($this);
        }

        return $this;
    }

    public function removeUserOperation(UserOperation $userOperation): self
    {
        if ($this->userOperations->contains($userOperation)) {
            $this->userOperations->removeElement($userOperation);
            // set the owning side to null (unless already changed)
            if ($userOperation->getUser() === $this) {
                $userOperation->setUser(null);
            }
        }

        return $this;
    }

    public function getUserNetwork(): ?UserNetwork
    {
        return $this->userNetwork;
    }

    public function setUserNetwork(UserNetwork $userNetwork): self
    {
        $this->userNetwork = $userNetwork;

        // set the owning side of the relation if necessary
        if ($this !== $userNetwork->getUser()) {
            $userNetwork->setUser($this);
        }

        return $this;
    }

    /**
     * @return Collection|UserNetworkBinaryTree[]
     */
    public function getUserNetworkBinaryTrees(): Collection
    {
        return $this->userNetworkBinaryTrees;
    }

    public function addUserNetworkBinaryTree(UserNetworkBinaryTree $userNetworkBinaryTree): self
    {
        if (!$this->userNetworkBinaryTrees->contains($userNetworkBinaryTree)) {
            $this->userNetworkBinaryTrees[] = $userNetworkBinaryTree;
            $userNetworkBinaryTree->setInvitedBy($this);
        }

        return $this;
    }

    public function removeUserNetworkBinaryTree(UserNetworkBinaryTree $userNetworkBinaryTree): self
    {
        if ($this->userNetworkBinaryTrees->contains($userNetworkBinaryTree)) {
            $this->userNetworkBinaryTrees->removeElement($userNetworkBinaryTree);
            // set the owning side to null (unless already changed)
            if ($userNetworkBinaryTree->getInvitedBy() === $this) {
                $userNetworkBinaryTree->setInvitedBy(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserProduct[]
     */
    public function getUserProducts(): Collection
    {
        return $this->userProducts;
    }

    public function addUserProduct(UserProduct $userProduct): self
    {
        if (!$this->userProducts->contains($userProduct)) {
            $this->userProducts[] = $userProduct;
            $userProduct->setUser($this);
        }

        return $this;
    }

    public function removeUserProduct(UserProduct $userProduct): self
    {
        if ($this->userProducts->contains($userProduct)) {
            $this->userProducts->removeElement($userProduct);
            // set the owning side to null (unless already changed)
            if ($userProduct->getUser() === $this) {
                $userProduct->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserInvite[]
     */
    public function getUserInvites(): Collection
    {
        return $this->userInvites;
    }

    public function addUserInvite(UserInvite $userInvite): self
    {
        if (!$this->userInvites->contains($userInvite)) {
            $this->userInvites[] = $userInvite;
            $userInvite->setUser($this);
        }

        return $this;
    }

    public function removeUserInvite(UserInvite $userInvite): self
    {
        if ($this->userInvites->contains($userInvite)) {
            $this->userInvites->removeElement($userInvite);
            // set the owning side to null (unless already changed)
            if ($userInvite->getUser() === $this) {
                $userInvite->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getAmazonProducts()
    {
        return $this->amazonProducts;
    }

    /**
     * @param AmazonProduct $amazonProduct
     * @return $this
     */
    public function addAmazonProduct(AmazonProduct $amazonProduct)
    {
        if (!$this->amazonProducts->contains($amazonProduct)) {
            $this->amazonProducts[] = $amazonProduct;
            $amazonProduct->setUser($this);
        }

        return $this;
    }

    /**
     * @param AmazonProduct $amazonProduct
     * @return $this
     */
    public function removeAmazonProduct(AmazonProduct $amazonProduct)
    {
        if ($this->amazonProducts->contains($amazonProduct)) {
            $this->amazonProducts->removeElement($amazonProduct);
            // set the owning side to null (unless already changed)
            if ($amazonProduct->getUser() === $this) {
                $amazonProduct->setUser(null);
            }
        }

        return $this;
    }

    public function getReferralLink(): ?UserReferralLink
    {
        return $this->referralLink;
    }

    public function setReferralLink(UserReferralLink $referralLink): self
    {
        $this->referralLink = $referralLink;

        // set the owning side of the relation if necessary
        if ($this !== $referralLink->getUserId()) {
            $referralLink->setUserId($this);
        }

        return $this;
    }

    public function getUserReferralLink(): ?UserReferralLink
    {
        return $this->userReferralLink;
    }

    public function setUserReferralLink(?UserReferralLink $userReferralLink): self
    {
        $this->userReferralLink = $userReferralLink;

        return $this;
    }

    /**
     * @return Collection|ResetPasswordRequest[]
     */
    public function getResetPasswordRequests(): Collection
    {
        return $this->resetPasswordRequests;
    }

    public function addResetPasswordRequest(ResetPasswordRequest $resetPasswordRequest): self
    {
        if (!$this->resetPasswordRequests->contains($resetPasswordRequest)) {
            $this->resetPasswordRequests[] = $resetPasswordRequest;
            $resetPasswordRequest->setUser($this);
        }

        return $this;
    }

    public function removeResetPasswordRequest(ResetPasswordRequest $resetPasswordRequest): self
    {
        if ($this->resetPasswordRequests->contains($resetPasswordRequest)) {
            $this->resetPasswordRequests->removeElement($resetPasswordRequest);
            // set the owning side to null (unless already changed)
            if ($resetPasswordRequest->getUser() === $this) {
                $resetPasswordRequest->setUser(null);
            }
        }

        return $this;
    }

}
