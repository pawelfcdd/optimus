<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;


/**
 * @ORM\Entity(repositoryClass="App\Repository\AmazonProductRepository")
 * @Vich\Uploadable()
 */
class AmazonProduct
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $buyStatus;

    /**
     * @ORM\Column(type="datetime")
     */
    private $productionFrom;

    /**
     * @ORM\Column(type="datetime")
     */
    private $productionTo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $trackingCode;

    /**
     * @ORM\Column(type="text")
     */
    private $information;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string | null
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="amazone_product_images", fileNameProperty="image")
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $referralLink;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    public $createdAt;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="amazonProducts")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\AmazonProductSalesData", mappedBy="amazonProduct")
     */
    private $salesData;

    public function __construct()
    {
        $this->salesData = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getTitle();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBuyStatus(): ?bool
    {
        return $this->buyStatus;
    }

    public function setBuyStatus(bool $buyStatus): self
    {
        $this->buyStatus = $buyStatus;

        return $this;
    }

    public function getProductionFrom(): ?\DateTimeInterface
    {
        return $this->productionFrom;
    }

    public function setProductionFrom(\DateTimeInterface $productionFrom): self
    {
        $this->productionFrom = $productionFrom;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getProductionTo()
    {
        return $this->productionTo;
    }

    /**
     * @param mixed $productionTo
     * @return AmazonProduct
     */
    public function setProductionTo($productionTo)
    {
        $this->productionTo = $productionTo;
        return $this;
    }

    public function getTrackingCode(): ?string
    {
        return $this->trackingCode;
    }

    public function setTrackingCode(string $trackingCode = null): self
    {
        $this->trackingCode = $trackingCode;

        return $this;
    }

    public function getInformation(): ?string
    {
        return $this->information;
    }

    public function setInformation(string $information): self
    {
        $this->information = $information;

        return $this;
    }

    public function getReferralLink(): ?string
    {
        return $this->referralLink;
    }

    public function setReferralLink(string $referralLink = null): self
    {
        $this->referralLink = $referralLink;

        return $this;
    }

    /**
     * @param File|null $image
     * @return $this
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->setCreatedAt();
        }

        return $this;
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getImage()
    {
        return $this->image;
    }

    /**
     * @ORM\PrePersist()
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();

        return $this;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return AmazonProduct
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSalesData()
    {
        return $this->salesData;
    }

    public function addSalesData(AmazonProductSalesData $amazonProductSalesData): self
    {
        if (!$this->salesData->contains($amazonProductSalesData)) {
            $this->salesData[] = $amazonProductSalesData;
            $amazonProductSalesData->setAmazonProduct($this);
        }

        return $this;
    }

    public function removeSalesData(AmazonProductSalesData $amazonProductSalesData): self
    {
        if ($this->salesData->contains($amazonProductSalesData)) {
            $this->salesData->removeElement($amazonProductSalesData);
            // set the owning side to null (unless already changed)
            if ($amazonProductSalesData->getAmazonProduct() === $this) {
                $amazonProductSalesData->setAmazonProduct(null);
            }
        }

        return $this;
    }

}
