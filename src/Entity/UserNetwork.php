<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserNetworkRepository")
 */
class UserNetwork
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="userNetwork", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserNetworkBinaryTree", mappedBy="userNetwork")
     */
    private $network;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProductNetwork", mappedBy="network")
     */
    private $productNetworks;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $activeBranch;

    public function __construct()
    {
        $this->network = new ArrayCollection();
        $this->productNetworks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|UserNetworkBinaryTree[]
     */
    public function getNetwork(): Collection
    {
        return $this->network;
    }

    public function addNetwork(UserNetworkBinaryTree $network): self
    {
        if (!$this->network->contains($network)) {
            $this->network[] = $network;
            $network->setUserNetwork($this);
        }

        return $this;
    }

    public function removeNetwork(UserNetworkBinaryTree $network): self
    {
        if ($this->network->contains($network)) {
            $this->network->removeElement($network);
            // set the owning side to null (unless already changed)
            if ($network->getUserNetwork() === $this) {
                $network->setUserNetwork(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ProductNetwork[]
     */
    public function getProductNetworks(): Collection
    {
        return $this->productNetworks;
    }

    public function addProductNetwork(ProductNetwork $productNetwork): self
    {
        if (!$this->productNetworks->contains($productNetwork)) {
            $this->productNetworks[] = $productNetwork;
            $productNetwork->setNetwork($this);
        }

        return $this;
    }

    public function removeProductNetwork(ProductNetwork $productNetwork): self
    {
        if ($this->productNetworks->contains($productNetwork)) {
            $this->productNetworks->removeElement($productNetwork);
            // set the owning side to null (unless already changed)
            if ($productNetwork->getNetwork() === $this) {
                $productNetwork->setNetwork(null);
            }
        }

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name = null): self
    {
        $this->name = $name;

        return $this;
    }

    public function getActiveBranch(): ?string
    {
        return $this->activeBranch;
    }

    public function setActiveBranch(string $activeBranch): self
    {
        $this->activeBranch = $activeBranch;

        return $this;
    }
}
