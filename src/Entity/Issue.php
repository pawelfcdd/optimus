<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\IssueRepository")
 * @Vich\Uploadable()
 * @ORM\HasLifecycleCallbacks()
 */
class Issue
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="issues")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $section;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="issues_images", fileNameProperty="image")
     * @var File
     */
    private $imageFile;

    /**
     * @var string | null
     * @ORM\Column(type="text", nullable=true)
     */
    private $issueAnswer = null;

    /**
     * @var \DateTime | null
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $answeredAt;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $createdAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getSection(): ?string
    {
        return $this->section;
    }

    public function setSection(string $section): self
    {
        $this->section = $section;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @param File|null $image
     * @throws \Exception
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->setCreatedAt();
        }
    }

    /**
     * @ORM\PrePersist()
     * @return $this
     * @throws \Exception
     */
    public function checkIsCreatedAtSet()
    {
        if (!$this->getCreatedAt()) {
            $this->setCreatedAt();
        }
        return $this;
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function setImage($image = null)
    {
        $this->image = $image;

        return $this;
    }

    public function getImage()
    {
        return $this->image;
    }

    /**
     * @return $this
     * @throws \Exception
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return string|null
     */
    public function getIssueAnswer(): ?string
    {
        return $this->issueAnswer;
    }

    /**
     * @param string|null $issueAnswer
     * @return Issue
     */
    public function setIssueAnswer(?string $issueAnswer): Issue
    {
        $this->issueAnswer = $issueAnswer;
        return $this;
    }

    /**
     * @ORM\PreUpdate()
     */
    public function setAnsweredAt()
    {
        $this->answeredAt = new \DateTime();
    }

    /**
     * @return \DateTime|null
     */
    public function getAnsweredAt()
    {
        return $this->answeredAt;
    }
}
