<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserInviteRepository")
 */
class UserInvite
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="userInvites")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product", inversedBy="userInvites")
     * @ORM\JoinColumn(nullable=false)
     */
    private $product;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", cascade={"persist", "remove"})
     */
    private $userInvited;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $inviteCode;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isProductBuy;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getUserInvited(): ?User
    {
        return $this->userInvited;
    }

    public function setUserInvited(?User $userInvited): self
    {
        $this->userInvited = $userInvited;

        return $this;
    }

    public function getInviteCode(): ?string
    {
        return $this->inviteCode;
    }

    public function setInviteCode(string $inviteCode): self
    {
        $this->inviteCode = $inviteCode;

        return $this;
    }

    public function getIsProductBuy(): ?bool
    {
        return $this->isProductBuy;
    }

    public function setIsProductBuy(bool $isProductBuy): self
    {
        $this->isProductBuy = $isProductBuy;

        return $this;
    }
}
