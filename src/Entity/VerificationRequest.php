<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VerificationRequestRepository")
 * @ORM\Table()
 * @Vich\Uploadable()
 */
class VerificationRequest
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var integer
     * @ORM\Column(type="integer", length=10)
     */
    private $user;

    /**
     * @var string | null
     * @ORM\Column(nullable=true)
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="verification_images", fileNameProperty="image")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var bool
     * @ORM\Column(type="boolean", name="status", nullable=true)
     */
    private $status;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getUser(): ?int
    {
        return $this->user;
    }

    /**
     * @param int $user
     * @return VerificationRequest
     */
    public function setUser(int $user): VerificationRequest
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @param File|null $image
     * @throws \Exception
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->setCreatedAt();
        }
    }

    /**
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return VerificationRequest
     * @throws \Exception
     */
    public function setCreatedAt(): VerificationRequest
    {
        $this->createdAt = new \DateTime();
        return $this;
    }

    /**
     * @return bool | null
     */
    public function isStatus(): ?bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     * @return VerificationRequest
     */
    public function setStatus(bool $status): VerificationRequest
    {
        $this->status = $status;
        return $this;
    }


}