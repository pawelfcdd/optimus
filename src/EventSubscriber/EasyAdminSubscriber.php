<?php

namespace App\EventSubscriber;

use App\Entity\AmazonProduct;
use App\Entity\AmazonProductSalesData;
use App\Entity\ProductNetwork;
use App\Entity\User;
use App\Entity\UserAccount;
use App\Entity\UserInvite;
use App\Entity\UserNetwork;
use App\Entity\UserNetworkBinaryTree;
use App\Entity\UserProduct;
use App\Entity\UserReferralLink;
use App\EventSubscriber\Handler\PrePersistHandler;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Event\EasyAdminEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class EasyAdminSubscriber implements EventSubscriberInterface
{
    /** @var EntityManagerInterface  */
    protected $em;

    /** @var UserProduct */
    protected $userProduct;

    protected $requestStack;

    /** @var GenericEvent */
    protected $event;

    private $userPasswordEncoder;

    public function __construct(EntityManagerInterface $entityManager, RequestStack $requestStack, UserPasswordEncoderInterface $userPasswordEncoder)
    {
        $this->em = $entityManager;
        $this->requestStack = $requestStack;
        $this->userPasswordEncoder = $userPasswordEncoder;
    }

    public static function getSubscribedEvents()
    {
        return array(
            EasyAdminEvents::PRE_PERSIST => ['prePersistHandler'],
            EasyAdminEvents::POST_UPDATE => ['postUpdateHandler'],
        );
    }

    /**
     * @param GenericEvent $event
     */
    public function prePersistHandler(GenericEvent $event)
    {
        $this->event = $event;
        $entity = $event->getSubject();

        switch (true) {
            case $entity instanceof AmazonProductSalesData:
                $this->addAmazonProductToProductStatistic($entity);
                break;
            case $entity instanceof AmazonProduct:
                $this->addUserToAmazonProduct($entity);
                break;
            case $entity instanceof UserAccount:
                $this->addUserToAccount($entity);
                break;
            case $entity instanceof User:
                $this->createAdminUser($entity);

        }
    }

    public function postUpdateHandler(GenericEvent $event)
    {
        $entity = $event->getSubject();

        switch (true) {
            case $entity instanceof UserProduct:
                $this->updateUserProductEvents($event);
                break;
            case $entity instanceof UserNetwork:
                break;
            default:
                return;
        }
    }

    /**
     * @param User $entity
     */
    public function createAdminUser(User $entity)
    {
        $rolesMap = [
            'Admin' => 'ROLE_SUPER_ADMIN',
            'Client' => 'ROLE_CLIENT',
        ];
        $entityForMap = $this->requestStack->getCurrentRequest()->query->get('entity');
        $userRole = $rolesMap[$entityForMap];
        $user = $this->em->getRepository(User::class)->findOneBy(['email' => $entity->getEmail()]);

        if (!$user) {
            $entity->setPassword($this->userPasswordEncoder->encodePassword($entity, $entity->getPassword()));
            $entity->setRoles([$userRole]);
            $this->event['entity'] = $entity;
        }

    }

    /**
     * @param AmazonProductSalesData $entity
     */
    public function addAmazonProductToProductStatistic(AmazonProductSalesData $entity)
    {
        $request = $this->requestStack->getCurrentRequest();
        $amazonProductId = $request->query->get('amazonProduct');
        if (!$amazonProductId) {
            // TODO: add logger and some error page
            return;
        }
        $amazonProduct = $this->em->getRepository(AmazonProduct::class)->findOneBy(['id' => $amazonProductId]);

        $entity->setAmazonProduct($amazonProduct);

        $this->event['entity'] = $entity;

    }

    public function addUserToAccount(UserAccount $account)
    {
        $request = $this->requestStack->getCurrentRequest();
        $userId = $request->query->get('userId');
        /** @var User $user */
        $user = $this->em->getRepository(User::class)
            ->findOneBy(['id' => $userId]);

        $account->setUser($user);

        $this->event['entity'] = $account;
    }


    public function addUserToAmazonProduct(AmazonProduct $entity)
    {
        $request = $this->requestStack->getCurrentRequest();

        $userId = $request->query->get('userId');

        if (!$userId) {
            // TODO: add logger and some error page
            return;
        }
        /** @var User $user */
        $user = $this->em->getRepository(User::class)->findOneBy(['id' => $userId]);
        $entity->setUser($user);

        $event['entity'] = $entity;

    }

    public function updateUserProductEvents(GenericEvent $event)
    {
        /** @var UserProduct $entity */
        $entity = $event->getSubject();

        if (!($entity instanceof UserProduct)) {
            return;
        }

        /** @var UserProduct userProduct */
        $this->userProduct = $entity;
        /** @var User $invitedUser */
        $invitedUser = $entity->getUser();
        /** @var UserInvite $invite */
        $invite = $this->em->getRepository(UserInvite::class)
            ->findOneBy(['userInvited' => $entity->getUser()]);

        if ($invite) {
            $invite->setIsProductBuy(1);
            $invitedByUser = $invite->getUser();
            $userNetwork = $this->em->getRepository(UserNetwork::class)->findOneBy([
                'user' => $invitedByUser,
            ]);

            if ($userNetwork) {
                $userNetworkBinaryTree = new UserNetworkBinaryTree();
                $userProductNetwork = new ProductNetwork();

                $userNetworkBinaryTree
                    ->setUserNetwork($userNetwork)
                    ->setTreeMember($invitedUser)
                    ->setInvitedBy($invitedByUser)
                    ->setTreeBranch($userNetwork->getActiveBranch());


                $userProductNetwork
                    ->setProduct($entity->getProduct())
                    ->setNetwork($userNetwork);

                $this->em->persist($userProductNetwork);
                $this->em->persist($userNetworkBinaryTree);
            }
        }

        if ($entity->getIsApproved()) {
            $owner = $entity->getUser();
            $referralLink = $owner->getReferralLink();
            $userNetwork = $owner->getUserNetwork();

            if (!$referralLink) {
                $referralLink = new UserReferralLink();
                $referralLink
                    ->setUserId($owner)
                    ->setReferralCode($this->generateInviteCode(15))
                ;

                $this->em->persist($referralLink);
            }

            if (!$userNetwork) {
                $userNetwork = new UserNetwork();
                $userNetworkName = strtolower($owner->getUsername() . '_' . $entity->getProduct()->getName() . '_' . '_binary_net');
                $userNetwork
                    ->setUser($owner)
                    ->setName($userNetworkName)
                    ->setActiveBranch('top')
                ;

                $this->em->persist($userNetwork);
            }
        }

        $this->em->flush();

        $event['entity'] = $entity;
    }

    /**
     * @param int $length
     * @return string
     */
    public function generateInviteCode(int $length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }


}
