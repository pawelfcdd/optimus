import Vue from 'vue';
import Products from '../components/Products'
import Catalog from '../components/Catalog'
import Reviews from '../components/Reviews'

// require('./../css/products.scss');

/**
 * Create a fresh Vue Application instance
 */
new Vue({
    el: '#app',
    components: {Products, Catalog, Reviews}
});
